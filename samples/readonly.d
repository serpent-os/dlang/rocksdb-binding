import std.stdio : writefln;
import rocksdb.database;
import rocksdb.options;

int main()
{
    // Initialize options
    auto opts = new DBOptions;
    opts.createIfMissing = true;
    opts.errorIfExists = false;

    // Open database
    string path = "testdb";
    auto rw = Database.open(opts, path);

    // Put some value
    string key = "alpha";
    rw.putString(key, "beta");
    writefln("rw.putString: %s=%s", key, rw.getString(key));

    // Open read-only connections while the read-write connection is still open
    auto ro1 = Database.openReadOnly(opts, path);
    auto ro2 = Database.openReadOnly(opts, path);

    // Read some value
    string value1 = ro1.getString(key);
    string value2 = ro2.getString(key);
    writefln("ro1.getString: %s=%s", key, value1);
    writefln("ro2.getString: %s=%s", key, value2);
    assert("beta" == value1 && value1 == value2);

    // Close connections
    rw.destroy();
    ro1.destroy();
    ro2.destroy();

    // Return success
    return 0;
}
