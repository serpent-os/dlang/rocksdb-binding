import std.conv : to;
import std.stdio : writefln;
import rocksdb.writebatch;
import rocksdb.database;
import rocksdb.env;
import rocksdb.options;

int main()
{
    // Initialize options
    auto opts = new DBOptions;
    opts.createIfMissing = true;
    opts.errorIfExists = false;

    // Open database
    auto db = Database.open(opts, "testdb");

    /* Operating with byte arrays */
    // Put a value into the database
    ubyte[] key = [0, 1];
    db.put(key, [1, 2, 3]);
    // Get a value out of the database
    ubyte[] value = db.get(key);
    writefln("db.get: %s=%s", key, value);
    // Delete a value form the database
    db.del(key);

    /* Operating with strings */
    // Put a value into the database
    string stringKey = "alpha";
    db.putString(stringKey, "beta");
    // Get a value out of the database
    string stringValue = db.getString(stringKey);
    writefln("db.getString: %s=%s", stringKey, stringValue);
    // Delete a value form the database
    db.delString(stringKey);

    /* Batch operations */
    // Put multiple values into the database at once
    auto batch = new WriteBatch;
    for (int i = 0; i < 10; i++)
    {
        batch.putString(i.to!string, i.to!string);
        writefln("db.putString: %s=%s", key, stringValue);
    }
    db.write(batch);

    // Iterate over the DB
    auto it = db.iter();
    foreach (key, value; it)
    {
        writefln("db.iter: %s=%s", cast(string) key, cast(string) value);
        db.del(key);
    }
    it.destroy();

    // Close the connection
    db.destroy();

    // Return success
    return 0;
}
