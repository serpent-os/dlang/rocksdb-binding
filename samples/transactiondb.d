import std.conv : to;
import std.stdio : writeln;
import rocksdb.writebatch;
import rocksdb.database;
import rocksdb.env;
import rocksdb.options;
import rocksdb.transaction;
import rocksdb.transactiondb;

int main()
{
    import std.exception : assertThrown;
    import std.file : rmdirRecurse;

    auto txnDB = TransactionDB.open("testdb");
    // Make sure database is cleaned up after the test
    scope (exit)
        "testdb".rmdirRecurse();

    // Begin a transaction and rollback half of it
    auto txn = txnDB.beginTransaction();
    txn.putString("key1", "value1");
    txn.putString("key2", "value2");
    txn.setSavepoint();
    txn.putString("key3", "value3");
    txn.putString("key4", "value4");
    txn.rollbackToSavepoint();
    txn.commit();
    txn.destroy();

    // Check if the transaction was succesful
    assert(txnDB.getString("key1") == "value1");
    assert(txnDB.getString("key2") == "value2");
    assert(txnDB.getString("key3") is null);
    assert(txnDB.getString("key4") is null);

    return 0;
}
