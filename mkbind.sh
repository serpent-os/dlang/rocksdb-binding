#!/bin/bash
set -e

# supply the path to a copy of rocksdb/include/c.h
# -- otherwise, the version installed on the system will be used.

ROCKSDB_INCLUDE_PATH="/usr/include/rocksdb"

if [[ -n "$1" ]]; then
    ROCKSDB_INCLUDE_PATH="$1"
else
    echo "No RocksDB include path supplied -- using default path."
fi
echo "Using RocksDB include path: ${ROCKSDB_INCLUDE_PATH}"

c_header="${ROCKSDB_INCLUDE_PATH}/c.h"
if [[ ! -f "$c_header" ]]; then
    cat << EOM

Could not find the RocksDB C header file ${ROCKSDB_INCLUDE_PATH}/c.h ?

Please install RocksDB or download RocksDB from

  https://github.com/facebook/rocksdb/archive/refs/tags/v6.22.1.tar.gz

and run this script with $0 <rocksdb_dir>/include/rocksdb

EOM
    exit 1
fi

version_header="${ROCKSDB_INCLUDE_PATH:?}/version.h"
if [[ ! -f "$version_header" ]]; then
  cat << EOM

Could not find the RocksDB version header file $version_header ?

Please install RocksDB or download RocksDB from

  https://github.com/facebook/rocksdb/archive/refs/tags/v6.22.1.tar.gz

and run this script with $0 <rocksdb_dir>/include/rocksdb

EOM
  exit 1
fi
version_major="$(grep -oP "ROCKSDB_MAJOR\s+\K[0-9]+$" "$version_header")"
version_minor="$(grep -oP "ROCKSDB_MINOR\s+\K[0-9]+$" "$version_header")"
version_patch="$(grep -oP "ROCKSDB_PATCH\s+\K[0-9]+$" "$version_header")"
version="$version_major.$version_minor.$version_patch"

echo "Checking that the 'dstep' DLang binding generator is present..."
if ! command -v dstep >/dev/null 2>&1; then
    cat << EOM

Could not find the 'dstep' DLang binding generator?

Please clone the 'dstep' project from

  https://https://github.com/jacob-carlborg/dstep.git

and build the 'dstep' binary from the clone root with 'dub build'.

Finallly, ensure that the newly built 'bin/dstep' is included in your \$PATH.
(see https://github.com/jacob-carlborg/dstep/issues/265)

EOM
fi

echo -e "\nGenerating RocksDB $version DLang bindings..."
tmp_file=binding.d
dstep -o "$tmp_file" \
  "$c_header" \
  -I"${ROCKSDB_INCLUDE_PATH:?}" \
  --rename-enum-members=true \
  --package rocksdb \
  --comments=true \
  --global-attribute '@nogc' \
  --global-attribute 'nothrow'
cat << EOM > src/rocksdb/binding.d
/*  Generated from RocksDB v$version headers on $(date -u +'%Y-%m-%d %H:%M UTC') */

$(cat "$tmp_file")
EOM
rm "$tmp_file"

echo "Formatting DLang bindings with scripts/update_format.sh..."
if [[ ! -x scripts/update_format.sh ]]; then
  cat << EOM
Could not format the DLang bindings?

Remember to run 'dfmt -i src/rocksdb/binding.d' before committing !!
EOM
    exit 1
fi
scripts/update_format.sh