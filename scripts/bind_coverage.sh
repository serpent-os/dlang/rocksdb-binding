#!/bin/bash
## Usage: bind_coverage.sh [options]
## Description: Print the percentage of C symbols covered by D code
## Options:
##  -a,--all   print all C symbols
##  -y,--yes   print C symbols that are covered
##  -n,--no    print C symbols that are not covered
##  -h,--help  display this message

# Parse options
for arg in "$@"; do
  case "$arg" in
  -a | --all)
    y=1
    n=1
    ;;
  -y | --yes) y=1 ;;
  -n | --no) n=1 ;;
  -h | *)
    sed -ne "s/^## //p" "$0"
    exit 1
    ;;
  esac
done

yes=0 # symbols covered
no=0  # symbols not covered

bind_file="src/rocksdb/binding.d"
sources="$(find src -type f ! -name "binding.d" -name "*.d")"

# Check which symbols are present in D code
symbols="$(grep -oP 'rocksdb_\w+(?=\(|;)' "$bind_file")"
IFS=$'\n'
for symbol in $symbols; do
  if grep "\W$symbol\W" $sources >/dev/null 2>&1; then
    if [ -n "$y" ]; then
      printf "%s \033[32;1m✔\033[0m\n" "$symbol"
    fi
    ((yes += 1))
  else
    if [ -n "$n" ]; then
      printf "%s \033[31;1m✘\033[0m\n" "$symbol"
    fi
    ((no += 1))
  fi
done

# Print symbol coverage
cov="$(echo "100 * $yes / ($yes + $no)" | bc -l)"
printf "\033[1m%s: %.2f%% covered\033[0m\n" "$bind_file" "$cov"
