#!/bin/bash
set -e

# Test all recent releases
rels="v6.6.3 v6.6.4 v6.7.3 v6.8.1 v6.10.1 v6.10.2 v6.11.4 v6.11.6 v6.12.6 v6.12.7 v6.13.3 v6.14.5 v6.14.6 v6.15.2 v6.15.4 v6.15.5 v6.16.3 v6.17.3 v6.16.4 v6.19.3 v6.20.3 v6.22.1 v6.23.3 v6.24.2 v6.25.1 v6.25.3 v6.26.0 v6.26.1 v6.27.3"
declare -A config_map
config_map=(
  ["v6.6.3"]="v6.6.3"
  ["v6.6.4"]="v6.6.3"
  ["v6.7.3"]="v6.6.3"
  ["v6.8.1"]="v6.6.3"
  ["v6.10.1"]="v6.6.3"
  ["v6.10.2"]="v6.6.3"
  ["v6.11.4"]="v6.11.4"
  ["v6.11.6"]="v6.11.4"
  ["v6.12.6"]="v6.11.4"
  ["v6.12.7"]="v6.11.4"
  ["v6.13.3"]="v6.13.3"
  ["v6.14.5"]="v6.13.3"
  ["v6.14.6"]="v6.13.3"
  ["v6.15.2"]="v6.13.3"
  ["v6.15.4"]="v6.13.3"
  ["v6.15.5"]="v6.13.3"
  ["v6.16.3"]="v6.13.3"
  ["v6.17.3"]="v6.13.3"
  ["v6.16.4"]="v6.13.3"
  ["v6.19.3"]="v6.13.3"
  ["v6.20.3"]="v6.13.3"
  ["v6.22.1"]="v6.22.1"
  ["v6.23.3"]="v6.22.1"
  ["v6.24.2"]="v6.22.1"
  ["v6.25.1"]="v6.22.1"
  ["v6.25.3"]="v6.22.1"
  ["v6.26.0"]="v6.22.1"
  ["v6.26.1"]="v6.22.1"
  ["v6.27.3"]="v6.22.1"
)
for rel in $rels; do
  conf="rocksdb-${config_map[$rel]}"
  echo "Testing RocksDB $rel with $conf"
  if [ ! -f ".rocksdb/$rel/librocksdb.so" ]; then echo "This version wasn't built yet!"; continue; fi
  export LIBRARY_PATH=".rocksdb/$rel"
  export LD_LIBRARY_PATH=".rocksdb/$rel"
  export PKG_CONFIG_PATH=".rocksdb/$rel"
  if dub test --config="$conf" --force >/dev/null; then
    printf "%s \033[32;1m✔\033[0m\n" "$rel"
  else
    printf "%s \033[31;1m✘\033[0m\n" "$rel"
  fi
done
