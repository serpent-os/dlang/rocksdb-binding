#!/bin/bash

mkdir -p .rocksdb

build() {
    local version=$1
    [ -d ".rocksdb/$version" ] || git clone \
        -b "$version" \
        --single-branch \
        --depth 1 \
        "https://github.com/facebook/rocksdb.git" \
        ".rocksdb/$version"
    pushd ".rocksdb/$version"
    [ -f "librocksdb.so" ] || CFLAGS="-Wno-range-loop-construct" make -j8 shared_lib
    [ -f "rocksdb.pc" ] || cat << EOF > "rocksdb.pc"
prefix=$PWD
exec_prefix=\${prefix}
includedir=\${prefix}/include
libdir=$PWD

Name: rocksdb
Description: An embeddable persistent key-value store for fast storage
Version: $version
Libs: -L\${libdir}  -ldl -lrocksdb
Libs.private: -lpthread -lrt -ldl -luring -lsnappy -lgflags -lz -lbz2 -llz4 -lzstd -lnuma -ltbb  -ljemalloc
Cflags: -I\${includedir} -std=c++11  -faligned-new -DHAVE_ALIGNED_NEW -DROCKSDB_PLATFORM_POSIX -DROCKSDB_LIB_IO_POSIX  -DOS_LINUX -fno-builtin-memcmp -DROCKSDB_IOURING_PRESENT -DROCKSDB_FALLOCATE_PRESENT -DSNAPPY -DGFLAGS=1 -DZLIB -DBZIP2 -DLZ4 -DZSTD -DNUMA -DTBB -DROCKSDB_MALLOC_USABLE_SIZE -DROCKSDB_PTHREAD_ADAPTIVE_MUTEX -DROCKSDB_BACKTRACE -DROCKSDB_RANGESYNC_PRESENT -DROCKSDB_SCHED_GETCPU_PRESENT -DROCKSDB_AUXV_GETAUXVAL_PRESENT -march=native   -DHAVE_SSE42  -DHAVE_PCLMUL  -DHAVE_AVX2  -DHAVE_BMI  -DHAVE_LZCNT -DHAVE_UINT128_EXTENSION -DROCKSDB_SUPPORT_THREAD_LOCAL -DROCKSDB_JEMALLOC -DJEMALLOC_NO_DEMANGLE  -isystem third-party/gtest-1.8.1/fused-src
EOF
    popd
}

# Build all recent releases
rels="v6.6.3 v6.6.4 v6.7.3 v6.8.1 v6.10.1 v6.10.2 v6.11.4 v6.11.6 v6.12.6 v6.12.7 v6.13.3 v6.14.5 v6.14.6 v6.15.2 v6.15.4 v6.15.5 v6.16.3 v6.17.3 v6.16.4 v6.19.3 v6.20.3 v6.22.1 v6.23.3 v6.24.2 v6.25.1 v6.25.3 v6.26.0 v6.26.1 v6.27.3"
for rel in $rels; do
    echo "Building $rel..."
    if ! build "$rel"; then
        echo "Error building $rel!"
        break
    fi
done
