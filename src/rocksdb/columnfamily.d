module rocksdb.columnfamily;

import rocksdb.binding;
import rocksdb.error : ensureRocks;
import rocksdb.options : ReadOptions, WriteOptions;
import rocksdb.iterator : Iterator;
import rocksdb.database : Database;
import rocksdb.queryable;

class ColumnFamily : Getable, Putable, Deletable, Iterable
{
    string name;
    Queryable q;

    package(rocksdb) this(rocksdb_column_family_handle_t* hndl, string name, Queryable q)
    {
        this.handle = hndl;
        this.name = name;
        this.q = q;
    }

    ~this()
    {
        if (this.handle)
        {
            this.handle.rocksdb_column_family_handle_destroy();
            this.handle.destroy();
        }
    }

    /**
     * Ensure the handle is always valid
     */
    invariant
    {
        assert(this.handle !is null);
    }

    Iterator iter(ReadOptions readOpts = null)
    {
        return this.q.iter(this, readOpts);
    }

    @disable Iterator iter(ColumnFamily cf, ReadOptions readOpts = null)
    {
        assert(cf == this);
        return this.q.iter(cf, readOpts);
    }

    void drop()
    {
        // TODO: Not supported by TransactionDB (raise exception there?)
    }

    /* TODO: not supported by TransactionDB
        ubyte[][] multiGet(ubyte[][] keys, ReadOptions opts = null)
        {
            return this.db.multiGet(keys, this, opts);
        }

        string[] multiGetString(string[] keys, ReadOptions opts = null)
        {
            return this.db.multiGetString(keys, this, opts);
        }
    */

    ubyte[] get(ubyte[] key, ReadOptions opts = null)
    {
        return this.q.get(key, this, opts);
    }

    @disable ubyte[] get(ubyte[] key, ColumnFamily family, ReadOptions opts = null)
    {
        assert(family == this);
        return this.q.get(key, this, opts);
    }

    void put(ubyte[] key, ubyte[] value, WriteOptions opts = null)
    {
        this.q.put(key, value, this, opts);
    }

    @disable void put(ubyte[] key, ubyte[] value, ColumnFamily family, WriteOptions opts = null)
    {
        assert(family == this);
        this.q.put(key, value, this, opts);
    }

    void del(ubyte[] key, WriteOptions opts = null)
    {
        this.q.del(key, this, opts);
    }

    @disable void del(ubyte[] key, ColumnFamily family, WriteOptions opts = null)
    {
        assert(family == this);
        this.q.del(key, this, opts);
    }

package:
    /// Handle referencing the C object
    rocksdb_column_family_handle_t* handle;
}

unittest
{
    import std.stdio : writefln;
    import std.datetime.stopwatch : benchmark;
    import std.conv : to;
    import std.file : rmdirRecurse;
    import std.algorithm.searching : startsWith;
    import rocksdb.options : DBOptions, CompressionType;

    writefln("Testing Column Families");

    // DB Options
    auto opts = new DBOptions;
    opts.createIfMissing = true;
    opts.errorIfExists = false;
    opts.compression = CompressionType.NONE;

    // Create the database (if it does not exist)
    string path = "cfTestDB1";
    scope (exit)
        path.rmdirRecurse();
    auto db = Database.open(opts, path);

    string[] columnFamilies = [
        "test", "test1", "test2", "test3", "test4", "wow",
    ];

    // create a bunch of column families
    foreach (cf; columnFamilies)
    {
        if ((cf in db.columnFamilies) is null)
        {
            db.createColumnFamily(cf);
        }
    }

    db.destroy();
    db = Database.open(opts, path);
    scope (exit)
        destroy(db);

    // Test column family listing
    assert(Database.listColumnFamilies(opts, path).length == columnFamilies.length + 1);

    void testColumnFamily(ColumnFamily cf, int times)
    {
        for (int i = 0; i < times; i++)
        {
            cf.putString(cf.name ~ i.to!string, i.to!string);
        }

        for (int i = 0; i < times; i++)
        {
            assert(cf.getString(cf.name ~ i.to!string) == i.to!string);
        }

        cf.withIter((iter) {
            foreach (key, value; iter)
            {
                assert(key.startsWith(cf.name));
            }
        });
    }

    foreach (name, cf; db.columnFamilies)
    {
        if (name == "default")
            continue;

        writefln("  %s", name);
        testColumnFamily(cf, 1000);
    }
}
