module rocksdb.database;

import std.conv : to;
import std.file : isDir, exists;
import std.array : array;
import std.stdio : writeln;
import std.string : fromStringz, toStringz;
import std.format : format;

import core.memory : GC;
import core.stdc.string : strlen;

import rocksdb.writebatch;
import rocksdb.binding;
import rocksdb.columnfamily : ColumnFamily;
import rocksdb.error : ensureRocks;
import rocksdb.iterator;
import rocksdb.options;
import rocksdb.queryable : Queryable, Snapshotable;
import rocksdb.snapshot : Snapshot;

/// A connection to a RocksDB instance
class Database : Queryable
{
    deprecated
    {
        /// Creates a database object and immediately opens the connection
        this(DBOptions dbOpts, string path, DBOptions[string] columnFamilies = null)
        {
            char* err = null;
            this.dbOptions = dbOpts;

            string[] existingColumnFamilies;

            // If there is an existing database we can check for existing column families
            if (exists(path) && isDir(path))
            {
                // First check if the database has any column families
                existingColumnFamilies = Database.listColumnFamilies(dbOpts, path);
            }

            if (columnFamilies || existingColumnFamilies.length >= 1)
            {
                immutable(char*)[] columnFamilyNames;
                rocksdb_options_t*[] columnFamilyOptions;

                foreach (k; existingColumnFamilies)
                {
                    columnFamilyNames ~= toStringz(k);

                    if ((k in columnFamilies) !is null)
                    {
                        columnFamilyOptions ~= columnFamilies[k].opts;
                    }
                    else
                    {
                        columnFamilyOptions ~= dbOpts.opts;
                    }
                }

                rocksdb_column_family_handle_t*[] result;
                result.length = columnFamilyNames.length;

                this.handle = rocksdb_open_column_families(dbOpts.opts, toStringz(path),
                        cast(int) columnFamilyNames.length, columnFamilyNames.ptr,
                        columnFamilyOptions.ptr, result.ptr, &err);

                foreach (i, h; result)
                {
                    string name = existingColumnFamilies[i];
                    this.columnFamilies[name] = new ColumnFamily(h, name, this);
                }
            }
            else
            {
                this.handle = rocksdb_open(dbOptions.opts, toStringz(path), &err);
            }

            err.ensureRocks();

            this.writeOptions = new WriteOptions;
            this.readOptions = new ReadOptions;
        }
    }

    /// Initialize a database object from a RocksDB handle
    package(rocksdb) this(rocksdb_t* hndl, DBOptions dbOpts)
    {
        this.handle = hndl;
        this.dbOptions = dbOpts;
        this.writeOptions = new WriteOptions;
        this.readOptions = new ReadOptions;
        this.columnFamilies = null;
    }

    /// Closes the connection and cleans up RocksDB handle
    ~this()
    {
        if (this.handle !is null)
        {
            foreach (k, cf; this.columnFamilies)
            {
                cf.destroy();
            }
            this.handle.rocksdb_close();
            this.handle.destroy();
        }
    }

    /**
     * Ensure the handle is always valid
     */
    invariant
    {
        assert(this.handle !is null);
    }

    /// Creates a database object and opens a read-write connection
    static Database open(string path)
    {
        DBOptions dbOpts = new DBOptions;
        dbOpts.createIfMissing = true;
        return Database.open(dbOpts, path);
    }

    /// Creates a database object and opens a read-write connection
    static Database open(DBOptions dbOpts, string path, DBOptions[string] columnFamilies = null)
    {
        return Database.open(dbOpts, path, columnFamilies, -1, false, false);
    }

    /// Creates a database object and opens a read-write connection with TTL
    static Database openWithTTL(string path, int ttl)
    {
        DBOptions dbOpts = new DBOptions;
        dbOpts.createIfMissing = true;
        return Database.openWithTTL(dbOpts, path, ttl);
    }

    /// Creates a database object and opens a read-write connection with TTL
    static Database openWithTTL(DBOptions dbOpts, string path, int ttl)
    {
        return Database.openWithTTL(dbOpts, path, null, ttl);
    }

    /// Creates a database object and opens a read-write connection with TTL
    // TODO: support TTL per CF
    static Database openWithTTL(DBOptions dbOpts, string path,
            DBOptions[string] columnFamilies, int ttl)
    {
        return Database.open(dbOpts, path, columnFamilies, ttl, false, false);
    }

    /// Creates a database object and opens a read-only connection
    static Database openReadOnly(string path, bool errorIfWALExists = false)
    {
        return Database.openReadOnly(new DBOptions, path, errorIfWALExists);
    }

    /// Creates a database object and opens a read-only connection
    static Database openReadOnly(DBOptions dbOpts, string path, bool errorIfWALExists = false)
    {
        return Database.openReadOnly(dbOpts, path, null, errorIfWALExists);
    }

    /// Creates a database object and opens a read-only connection
    static Database openReadOnly(DBOptions dbOpts, string path,
            DBOptions[string] columnFamilies, bool errorIfWALExists = false)
    {
        return Database.open(dbOpts, path, columnFamilies, -1, errorIfWALExists, true);
    }

    /// Open a connection
    // TODO: Cleanup TTL args, has to have same length as columnFamilies (in dbopts maybe?)
    protected static Database open(DBOptions dbOpts, string path,
            DBOptions[string] columnFamilies, int ttl, bool errorIfWALExists, bool readOnly)
    {
        rocksdb_t* dbHndl = null;
        char* err = null;
        string[] existingColumnFamilies;

        // If there is an existing database we can check for existing column families
        if (exists(path) && isDir(path))
        {
            // First check if the database has any column families
            existingColumnFamilies = Database.listColumnFamilies(dbOpts, path);
        }

        if (columnFamilies || existingColumnFamilies.length >= 1)
        {
            immutable(char*)[] columnFamilyNames;
            rocksdb_options_t*[] columnFamilyOptions;
            int[] columnFamilyTTLs;
            foreach (k; existingColumnFamilies)
            {
                columnFamilyNames ~= toStringz(k);
                columnFamilyOptions ~= columnFamilies.get(k, dbOpts).opts;
                columnFamilyTTLs ~= ttl; // TODO: TTL per CF
            }

            rocksdb_column_family_handle_t*[] result;
            result.length = columnFamilyNames.length;
            if (!readOnly && ttl < 0)
            {
                dbHndl = rocksdb_open_column_families(dbOpts.opts, toStringz(path), cast(int) columnFamilyNames.length,
                        columnFamilyNames.ptr, columnFamilyOptions.ptr, result.ptr, &err);
            }
            else if (!readOnly)
            {
                version (RocksDBOpenColumnFamiliesWithTTL)
                {
                    dbHndl = rocksdb_open_column_families_with_ttl(dbOpts.opts,
                            toStringz(path), cast(int) columnFamilyNames.length,
                            columnFamilyNames.ptr, columnFamilyOptions.ptr,
                            result.ptr, columnFamilyTTLs.ptr, &err);
                }
                else
                {
                    throw new Exception("rocksdb_open_column_families_with_ttl is not implemented");
                }
            }
            else
            {
                dbHndl = rocksdb_open_for_read_only_column_families(dbOpts.opts,
                        toStringz(path), cast(int) columnFamilyNames.length,
                        columnFamilyNames.ptr, columnFamilyOptions.ptr,
                        result.ptr, errorIfWALExists, &err);
            }
            err.ensureRocks();

            Database db = new Database(dbHndl, dbOpts);
            foreach (i, h; result)
            {
                string name = existingColumnFamilies[i];
                db.columnFamilies[name] = new ColumnFamily(h, name, db);
            }
            return db;
        }

        // Open database without specifying column families
        if (!readOnly && ttl < 0)
        {
            dbHndl = rocksdb_open(dbOpts.opts, toStringz(path), &err);
        }
        else if (!readOnly)
        {

            dbHndl = rocksdb_open_with_ttl(dbOpts.opts, toStringz(path), ttl, &err);
        }
        else
        {
            dbHndl = rocksdb_open_for_read_only(dbOpts.opts, toStringz(path),
                    errorIfWALExists, &err);
        }
        err.ensureRocks();
        return new Database(dbHndl, dbOpts);
    }

    ColumnFamily createColumnFamily(string name, DBOptions dbOpts = null)
    {
        char* err = null;

        auto cfh = this.handle.rocksdb_create_column_family((dbOpts ? dbOpts
                : this.dbOptions).opts, toStringz(name), &err);
        err.ensureRocks();

        return this.columnFamilies[name] = new ColumnFamily(cfh, name, this);
    }

    void dropColumnFamily(ColumnFamily cf)
    {
        char* err = null;
        this.handle.rocksdb_drop_column_family(cf.handle, &err);
        err.ensureRocks();
    }

    // TODO: TransactionDB can use this
    static string[] listColumnFamilies(DBOptions dbOpts, string path)
    {
        char* err = null;
        size_t numColumnFamilies;

        char** columnFamilies = rocksdb_list_column_families(dbOpts.opts,
                toStringz(path), &numColumnFamilies, &err);

        err.ensureRocks();

        string[] result = new string[](numColumnFamilies);

        // Iterate over and convert/copy all column family names
        for (size_t i = 0; i < numColumnFamilies; i++)
        {
            result[i] = fromStringz(columnFamilies[i]).to!string;
        }

        rocksdb_list_column_families_destroy(columnFamilies, numColumnFamilies);

        return result;
    }

    /**
     * Create a snapshot
     */
    Snapshot getSnapshot()
    {
        auto sHandle = this.handle.rocksdb_create_snapshot();
        return new Snapshot(sHandle, this);
    }

    /**
     * Release a snapshot (used in the destructor of Snapshot)
     */
    void releaseSnapshot(Snapshot snapshot)
    {
        this.handle.rocksdb_release_snapshot(snapshot.handle);
    }

    /**
     * Create an openable snapshot
     */
    void createCheckpoint(string checkpointDir, ulong logSizeForFlush = 0)
    {
        char* err;
        auto chkpt = this.handle.rocksdb_checkpoint_object_create(&err);
        err.ensureRocks();
        chkpt.rocksdb_checkpoint_create(checkpointDir.ptr, logSizeForFlush, &err);
        err.ensureRocks();
        chkpt.rocksdb_checkpoint_object_destroy();
    }

    ubyte[] get(ubyte[] key, ReadOptions readOpts = null)
    {
        char* err;
        size_t vlen;
        ubyte* val = cast(ubyte*) this.handle.rocksdb_get((readOpts ? readOpts
                : this.readOptions).opts, cast(char*) key.ptr, key.length, &vlen, &err);
        err.ensureRocks();
        GC.addRange(val, vlen);
        return cast(ubyte[]) val[0 .. vlen];
    }

    ubyte[] get(ubyte[] key, ColumnFamily family, ReadOptions readOpts = null)
    {
        char* err;
        size_t vlen;
        ubyte* val = cast(ubyte*) this.handle.rocksdb_get_cf((readOpts ? readOpts
                : this.readOptions).opts, family.handle, cast(char*) key.ptr,
                key.length, &vlen, &err);
        err.ensureRocks();
        GC.addRange(val, vlen);
        return cast(ubyte[]) val[0 .. vlen];
    }

    ubyte[][] multiGet(ubyte[][] keys, ColumnFamily family = null, ReadOptions readOpts = null)
    {
        char*[] ckeys = new char*[](keys.length);
        size_t[] ckeysSizes = new size_t[](keys.length);

        foreach (idx, key; keys)
        {
            ckeys[idx] = cast(char*) key;
            ckeysSizes[idx] = key.length;
        }

        char*[] vals = new char*[](keys.length);
        size_t[] valsSizes = new size_t[](keys.length);
        char*[] errs = new char*[](keys.length);

        if (family)
        {
            this.handle.rocksdb_multi_get_cf((readOpts ? readOpts : this.readOptions)
                    .opts, cast(const)&family.handle, keys.length, ckeys.ptr,
                    ckeysSizes.ptr, vals.ptr, valsSizes.ptr, errs.ptr);
        }
        else
        {
            this.handle.rocksdb_multi_get((readOpts ? readOpts : this.readOptions)
                    .opts, keys.length, ckeys.ptr, ckeysSizes.ptr, vals.ptr,
                    valsSizes.ptr, errs.ptr);
        }

        ubyte[][] result = new ubyte[][](keys.length);
        for (int idx = 0; idx < ckeys.length; idx++)
        {
            errs[idx].ensureRocks();
            result[idx] = cast(ubyte[]) vals[idx][0 .. valsSizes[idx]];
        }

        return result;
    }

    string[] multiGetString(string[] keys, ColumnFamily family = null, ReadOptions readOpts = null)
    {
        char*[] ckeys = new char*[](keys.length);
        size_t[] ckeysSizes = new size_t[](keys.length);

        foreach (idx, key; keys)
        {
            ckeys[idx] = cast(char*) key.ptr;
            ckeysSizes[idx] = key.length;
        }

        char*[] vals = new char*[](keys.length);
        size_t[] valsSizes = new size_t[](keys.length);
        char*[] errs = new char*[](keys.length);

        if (family)
        {
            this.handle.rocksdb_multi_get_cf((readOpts ? readOpts : this.readOptions)
                    .opts, cast(const)&family.handle, keys.length, ckeys.ptr,
                    ckeysSizes.ptr, vals.ptr, valsSizes.ptr, errs.ptr);
        }
        else
        {
            this.handle.rocksdb_multi_get((readOpts ? readOpts : this.readOptions)
                    .opts, keys.length, ckeys.ptr, ckeysSizes.ptr, vals.ptr,
                    valsSizes.ptr, errs.ptr);
        }

        string[] result = new string[](keys.length);
        for (int idx = 0; idx < ckeys.length; idx++)
        {
            errs[idx].ensureRocks();
            result[idx] = cast(string) vals[idx][0 .. valsSizes[idx]];
        }

        return result;
    }

    void put(ubyte[] key, ubyte[] val, WriteOptions writeOpts = null)
    {
        char* err;
        this.handle.rocksdb_put((writeOpts ? writeOpts : this.writeOptions).opts,
                cast(char*) key.ptr, key.length, cast(char*) val.ptr, val.length, &err);
        err.ensureRocks();
    }

    void put(ubyte[] key, ubyte[] val, ColumnFamily cf, WriteOptions writeOpts = null)
    {
        char* err;
        this.handle.rocksdb_put_cf((writeOpts ? writeOpts : this.writeOptions)
                .opts, cf.handle, cast(char*) key.ptr, key.length,
                cast(char*) val.ptr, val.length, &err);
        err.ensureRocks();
    }

    void del(ubyte[] key, WriteOptions writeOpts = null)
    {
        char* err;
        this.handle.rocksdb_delete((writeOpts ? writeOpts : this.writeOptions)
                .opts, cast(char*) key.ptr, key.length, &err);
        err.ensureRocks();
    }

    void del(ubyte[] key, ColumnFamily cf, WriteOptions writeOpts = null)
    {
        char* err;
        this.handle.rocksdb_delete_cf((writeOpts ? writeOpts : this.writeOptions)
                .opts, cf.handle, cast(char*) key.ptr, key.length, &err);
        err.ensureRocks();
    }

    // TODO: check if this still works the same
    deprecated
    {
        void remove(ubyte[] key, ColumnFamily cf, WriteOptions writeOpts = null)
        {
            if (!cf)
            {
                this.del(key, writeOpts);
            }
            else
            {
                this.del(key, cf, writeOpts);
            }
        }
    }

    void write(WriteBatch batch, WriteOptions writeOpts = null)
    {
        char* err;
        this.handle.rocksdb_write((writeOpts ? writeOpts : this.writeOptions)
                .opts, batch.handle, &err);
        err.ensureRocks();
    }

    void merge(ubyte[] key, ubyte[] val, WriteOptions writeOpts = null)
    {
        char* err;
        this.handle.rocksdb_merge((writeOpts ? writeOpts : this.writeOptions)
                .opts, cast(char*) key.ptr, key.length, cast(char*) val.ptr, val.length, &err);
        err.ensureRocks();
    }

    void merge(ubyte[] key, ubyte[] val, ColumnFamily cf, WriteOptions writeOpts = null)
    {
        char* err;
        this.handle.rocksdb_merge_cf((writeOpts ? writeOpts : this.writeOptions)
                .opts, cf.handle, cast(char*) key.ptr, key.length,
                cast(char*) val.ptr, val.length, &err);
        err.ensureRocks();
    }

    Iterator iter(ReadOptions readOpts = null)
    {
        auto it = this.handle.rocksdb_create_iterator((readOpts ? readOpts : this.readOptions).opts);
        return new Iterator(it);
    }

    Iterator iter(ColumnFamily cf, ReadOptions readOpts = null)
    {
        auto it = this.handle.rocksdb_create_iterator_cf((readOpts ? readOpts
                : this.readOptions).opts, cf.handle);
        return new Iterator(it);
    }

    void flush(FlushOptions flushOpts)
    {
        char* err;
        this.handle.rocksdb_flush(flushOpts.handle, &err);
        err.ensureRocks();
    }

    void flush(ColumnFamily cf, FlushOptions flushOpts)
    {
        char* err;
        this.handle.rocksdb_flush_cf(flushOpts.handle, cf.handle, &err);
        err.ensureRocks();
    }

    version (RocksDBFlushWALSupport)
    {
        void flushWAL(bool sync)
        {
            char* err;
            this.handle.rocksdb_flush_wal(sync, &err);
            err.ensureRocks();
        }
    }

package:
    /// Handle referencing the C object
    rocksdb_t* handle;

    /// Options for the database
    DBOptions dbOptions;
    /// Default options for write operations
    WriteOptions writeOptions;
    /// Default options for read operations
    ReadOptions readOptions;

    /// Selected column families for this connection
    ColumnFamily[string] columnFamilies;
}

/// Test read-only database
unittest
{
    import core.exception : AssertError;
    import std.exception : assertThrown;
    import std.file : rmdirRecurse;
    import rocksdb.env : Env;

    auto env = new Env;
    env.backgroundThreads = 2;
    env.highPriorityBackgroundThreads = 1;

    auto dbOpts = new DBOptions;
    dbOpts.createIfMissing = true;
    dbOpts.errorIfExists = false;
    dbOpts.compression = CompressionType.NONE;
    dbOpts.env = env;

    // Make sure database is clean

    // Open read-write connection
    auto rw = Database.open(dbOpts, "testDB2");
    scope (exit)
        "testDB2".rmdirRecurse();

    // Write some vals
    rw.put([0], [1, 2, 3]);
    rw.put([1], [4, 5, 6]);
    rw.putString("a", "test1");
    rw.putString("b", "test2");

    // Cleanup database
    destroy(rw);

    // Open read-only connection
    auto ro = Database.openReadOnly(dbOpts, "testDB2");

    // Read some vals
    assert(ro.get([0]) == [1, 2, 3]);
    assert(ro.get([1]) == [4, 5, 6]);
    assert(ro.getString("a") == "test1");
    assert(ro.getString("b") == "test2");

    // Write some vals
    assertThrown!Exception(ro.put([0], [1, 2, 3]));
    assertThrown!Exception(ro.put([1], [4, 5, 6]));
    assertThrown!Exception(ro.putString("a", "test1"));
    assertThrown!Exception(ro.putString("b", "test2"));

    // Cleanup database
    destroy(ro);
}
