module rocksdb.slicetransform;

import rocksdb.binding;

public import rocksdb.slice : Slice;

/**
 * Wrapper around SliceTransform
 */
public abstract class SliceTransform
{

    /**
     * Return the underlying SliceTransform pointer
     */
    pure @property rocksdb_slicetransform_t* rst() @nogc nothrow
    {
        return _rst;
    }

    /**
     * Super constructor for custom implementations
     */
    this(const(string) clzName)
    {
        import std.exception : enforce;

        enforce(clzName !is null, "rocksdb.SliceTransform: Name must not me null");
        this.clzName = clzName;
        _rst = rocksdb_slicetransform_create(cast(void*) this,
                &d_sliceTransformDestructor, &d_sliceTransformTransform,
                &d_sliceTransformInDomain, &d_sliceTransformInRange, &d_sliceTransformName);
    }

    /**
     * We have to assume we're bound to an Options which will explicitly
     * destroy the handle anyway, otherwise we'll cause segfaults. Right
     * now there is no nice way to handle this.
     */
    ~this() @nogc nothrow
    {
        _rst = null;
    }

    /**
     * Return true if this slice is compatible with our transform logic
     */
    abstract bool inDomain(const Slice domain)
    {
        return false;
    }

    /**
     * Return true if this slice is within our range
     */
    abstract bool inRange(const Slice range)
    {
        return false;
    }

    /**
     * Return a transformed Slice for the prefix from the input slice.
     */
    abstract Slice transform(const Slice inpSlice)
    {
        return inpSlice;
    }

    /**
     * Run the destructor on the SliceTransform
     */
    extern (C) static void d_sliceTransformDestructor(void* p)
    {
        auto st = cast(SliceTransform) p;
        st.destroy();
    }

    /**
     * Run the transform method on the SliceTransform
     */
    extern (C) static char* d_sliceTransformTransform(void* p,
            const(char)* key, size_t length, size_t* dstLength)
    {
        auto st = cast(SliceTransform) p;
        auto inpSlice = Slice.fromChar(length, key);
        auto outpSlice = st.transform(inpSlice);
        *dstLength = outpSlice.l;
        return cast(char*) outpSlice.p;
    }

    /**
     * Run the inDomain method on the SliceTransform
     */
    extern (C) static ubyte d_sliceTransformInDomain(void* p, const(char)* key, size_t length)
    {
        auto st = cast(SliceTransform) p;
        auto inpSlice = Slice.fromChar(length, key);
        return st.inDomain(inpSlice);
    }

    /**
     * Run the inRange method on the SliceTransform
     */
    extern (C) static ubyte d_sliceTransformInRange(void* p, const(char)* key, size_t length)
    {
        auto st = cast(SliceTransform) p;
        auto inpSlice = Slice.fromChar(length, key);
        return st.inRange(inpSlice);
    }

    /**
     * Extract the name of the SliceTransform
     */
    extern (C) static char* d_sliceTransformName(void* p)
    {
        import std.string : toStringz;

        auto st = cast(SliceTransform) p;
        return cast(char*) toStringz(st.clzName);
    }

protected:

    /**
     * Only our implementations use this constructor for their own API calls
     */
    this() @nogc nothrow
    {

    }

    /**
     * Update the private handle
     */
    pure @property void rst(rocksdb_slicetransform_t* rst) @nogc nothrow
    {
        _rst = rst;
    }

private:

    rocksdb_slicetransform_t* _rst = null;
    string clzName = null;
}

/**
 * Wrapper around FixedPrefix SliceTransform
 */
public final class FixedPrefixSliceTransform : SliceTransform
{
    @disable this();

    /**
     * Construct a new FixedPrefixSliceTransform with nBytes as the size
     * of prefixes
     */
    this(size_t nBytes) @nogc nothrow
    {
        rst = rocksdb_slicetransform_create_fixed_prefix(nBytes);
    }

    /**
     * Never used
     */
    override Slice transform(const Slice inp)
    {
        return inp;
    }

    /**
     * Never used
     */
    override bool inDomain(const Slice inp)
    {
        return false;
    }

    /**
     * Never used
     */
    override bool inRange(const Slice inp)
    {
        return false;
    }
}

/**
 * Wrapper around Noop SliceTransform
 */
public final class NoopSliceTransform : SliceTransform
{

    /**
     * Construct a new Noop SliceTransform
     */
    this() @nogc nothrow
    {
        rst = rocksdb_slicetransform_create_noop();
    }

    /**
     * Never used
     */
    override Slice transform(const Slice inp)
    {
        return inp;
    }

    /**
     * Never used
     */
    override bool inDomain(const Slice inp)
    {
        return false;
    }

    /**
     * Never used
     */
    override bool inRange(const Slice inp)
    {
        return false;
    }
}
