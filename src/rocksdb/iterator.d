module rocksdb.iterator;

import std.conv : to;
import std.string : fromStringz, toStringz;

import rocksdb.binding;
import rocksdb.slice : Slice;
import rocksdb.options : ReadOptions;
import rocksdb.database : Database;
import rocksdb.columnfamily : ColumnFamily;

class Iterator
{
    package(rocksdb) this(rocksdb_iterator_t* hndl)
    {
        this.handle = hndl;
        this.seekToFirst();
    }

    ~this()
    {
        if (this.handle)
        {
            this.handle.rocksdb_iter_destroy();
            this.handle.destroy();
        }
    }

    /**
     * Ensure the handle is always valid
     */
    invariant
    {
        assert(this.handle !is null);
    }

    void seekToFirst()
    {
        this.handle.rocksdb_iter_seek_to_first();
    }

    void seekToLast()
    {
        this.handle.rocksdb_iter_seek_to_last();
    }

    void seek(string key)
    {
        this.seek(cast(ubyte[]) key);
    }

    void seek(in ubyte[] key)
    {
        this.handle.rocksdb_iter_seek(cast(char*) key.ptr, key.length);
    }

    void seekPrev(string key)
    {
        this.seekPrev(cast(ubyte[]) key);
    }

    void seekPrev(in ubyte[] key)
    {
        this.handle.rocksdb_iter_seek_for_prev(cast(char*) key.ptr, key.length);
    }

    void next()
    {
        this.handle.rocksdb_iter_next();
    }

    void prev()
    {
        this.handle.rocksdb_iter_prev();
    }

    bool valid()
    {
        return cast(bool) this.handle.rocksdb_iter_valid();
    }

    ubyte[] key()
    {
        size_t size;
        const(char)* ckey = this.handle.rocksdb_iter_key(&size);
        return cast(ubyte[]) ckey[0 .. size];
    }

    Slice keySlice()
    {
        size_t size;
        const(char)* ckey = this.handle.rocksdb_iter_key(&size);
        return Slice.fromChar(size, ckey);
    }

    ubyte[] value()
    {
        size_t size;
        const(char)* cvalue = this.handle.rocksdb_iter_value(&size);
        return cast(ubyte[]) cvalue[0 .. size];
    }

    Slice valueSlice()
    {
        size_t size;
        const(char)* cvalue = this.handle.rocksdb_iter_value(&size);
        return Slice.fromChar(size, cvalue);
    }

    int opApply(scope int delegate(ubyte[], ubyte[]) dg)
    {
        int result = 0;

        while (this.valid())
        {
            result = dg(this.key(), this.value());
            if (result)
            {
                break;
            }
            this.next();
        }

        return result;
    }

package:
    /// Handle referencing the C object
    rocksdb_iterator_t* handle;
}
