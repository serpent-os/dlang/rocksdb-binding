module rocksdb.transaction;

import core.memory : GC;

import rocksdb.writebatch : WriteBatch;
import rocksdb.binding;
import rocksdb.columnfamily : ColumnFamily;
import rocksdb.error : ensureRocks;
import rocksdb.options : ReadOptions, WriteOptions;
import rocksdb.iterator : Iterator;
import rocksdb.queryable;
import rocksdb.snapshot : Snapshot;

/**
 * Options for a single Transaction
 */
class TransactionOptions
{
    /**
     * Initialize a new handle
     */
    this()
    {
        this.handle = rocksdb_transaction_options_create();
    }

    /**
     * Destroy the handle
     */
    ~this()
    {
        if (this.handle)
        {
            this.handle.rocksdb_transaction_options_destroy();
            this.handle.destroy();
        }
    }

    /**
     * Ensure the handle is always valid
     */
    invariant
    {
        assert(this.handle !is null);
    }

    /**
     * Setting setSnapshot to true is the same as calling Transaction.setSnapshot()
     */
    @property void setSnapshot(bool ss)
    {
        this.handle.rocksdb_transaction_options_set_set_snapshot(ss);
    }

    /**
     * Setting to true means that before acquiring locks, this transaction will
     * check if doing so will cause a deadlock. If so, it will return with
     * Status.Busy.  The user should retry their transaction.
     */
    @property void deadlockDetect(bool dd)
    {
        this.handle.rocksdb_transaction_options_set_deadlock_detect(dd);
    }

    /**
     * If positive, specifies the wait timeout in milliseconds when
     * a transaction attempts to lock a key.
     * 
     * If 0, no waiting is done if a lock cannot instantly be acquired.
     * If negative, TransactionDBOptions::transaction_lock_timeout will be used.
     */
    @property void lockTimeout(long lt)
    {
        this.handle.rocksdb_transaction_options_set_lock_timeout(lt);
    }

    /**
     * Expiration duration in milliseconds. If non-negative, transactions that
     * last longer than this many milliseconds will fail to commit.  If not set,
     * a forgotten transaction that is never committed, rolled back, or deleted
     * will never relinquish any locks it holds.  This could prevent keys from
     * being written by other writers.
     */
    @property void expiration(long e)
    {
        this.handle.rocksdb_transaction_options_set_expiration(e);
    }

    /**
     * The number of traversals to make during deadlock detection.
     */
    @property void deadlockDetectDepth(long d)
    {
        this.handle.rocksdb_transaction_options_set_deadlock_detect_depth(d);
    }

    /**
     * TODO
     */
    @property void maxWriteBatchSize(size_t mwbs)
    {
        this.handle.rocksdb_transaction_options_set_max_write_batch_size(mwbs);
    }

package:
    /// Handle referencing the C object
    rocksdb_transaction_options_t* handle;
}

/**
 * Options for a single OptimisticTransaction
 */
class OptimisticTransactionOptions
{
    /**
     * Initialize a new handle
     */
    this()
    {
        this.handle = rocksdb_optimistictransaction_options_create();
    }

    /**
     * Destroy the handle
     */
    ~this()
    {
        if (this.handle)
        {
            this.handle.rocksdb_optimistictransaction_options_destroy();
            this.handle.destroy();
        }
    }

    /**
     * Ensure the handle is always valid
     */
    invariant
    {
        assert(this.handle !is null);
    }

    /**
     * Setting setSnapshot to true is the same as calling Transaction.setSnapshot()
     */
    @property void setSnapshot(bool ss)
    {
        this.handle.rocksdb_optimistictransaction_options_set_set_snapshot(ss);
    }

package:
    /// Handle referencing the C object
    rocksdb_optimistictransaction_options_t* handle;
}

/**
 * A single database transaction
 */
class Transaction : Getable, Putable, Deletable, Mergeable, Iterable, Snapshotable
{

    /**
     * Create transaction from an existing handle
     */
    package(rocksdb) this(rocksdb_transaction_t* hndl)
    {
        this.handle = hndl;
    }

    /**
     * Destroy the handle
     */
    ~this()
    {
        if (this.handle)
        {
            this.handle.rocksdb_transaction_destroy();
            this.handle.destroy();
        }
    }

    /**
     * Ensure the handle is always valid
     */
    invariant
    {
        assert(this.handle !is null);
    }

    /**
     * Commit this transaction
     */
    void commit()
    {
        char* err;
        this.handle.rocksdb_transaction_commit(&err);
        err.ensureRocks();
    }

    /**
     * Rollback this transaction
     */
    void rollback()
    {
        char* err;
        this.handle.rocksdb_transaction_rollback(&err);
        err.ensureRocks();
    }

    /**
     * Set a new savepoint
     */
    void setSavepoint()
    {
        this.handle.rocksdb_transaction_set_savepoint();
    }

    /**
     * Rollback to the previous savepoint
     */
    void rollbackToSavepoint()
    {
        char* err;
        this.handle.rocksdb_transaction_rollback_to_savepoint(&err);
        err.ensureRocks();
    }

    /**
     * Create a snapshot
     */
    Snapshot getSnapshot()
    {
        auto sHandle = this.handle.rocksdb_transaction_get_snapshot();
        return new Snapshot(sHandle, this);
    }

    /**
     * Release a snapshot (used in the destructor of Snapshot)
     */
    void releaseSnapshot(Snapshot snapshot)
    {
        (cast(void*) snapshot.handle).rocksdb_free();
    }

    ubyte[] get(ubyte[] key, ReadOptions readOpts = null)
    {
        char* err;
        size_t vlen;
        ubyte* val = cast(ubyte*) this.handle.rocksdb_transaction_get((readOpts
                ? readOpts : this.readOptions).opts, cast(char*) key.ptr,
                key.length, &vlen, &err);
        err.ensureRocks();
        GC.addRange(val, vlen);
        return cast(ubyte[]) val[0 .. vlen];
    }

    ubyte[] get(ubyte[] key, ColumnFamily cf, ReadOptions readOpts = null)
    {
        char* err;
        size_t vlen;
        ubyte* val = cast(ubyte*) this.handle.rocksdb_transaction_get_cf((readOpts
                ? readOpts : this.readOptions).opts, cf.handle,
                cast(char*) key.ptr, key.length, &vlen, &err);
        err.ensureRocks();
        GC.addRange(val, vlen);
        return cast(ubyte[]) val[0 .. vlen];
    }

    ubyte[] getForUpdate(ubyte[] key, bool exclusive, ReadOptions readOpts = null)
    {
        char* err;
        size_t vlen;
        ubyte* val = cast(ubyte*) this.handle.rocksdb_transaction_get_for_update((readOpts
                ? readOpts : this.readOptions).opts, cast(char*) key.ptr,
                key.length, &vlen, exclusive, &err);
        err.ensureRocks();
        GC.addRange(val, vlen);
        return cast(ubyte[]) val[0 .. vlen];
    }

    ubyte[] getForUpdate(ubyte[] key, bool exclusive, ColumnFamily cf, ReadOptions readOpts = null)
    {
        char* err;
        size_t vlen;
        ubyte* val = cast(ubyte*) this.handle.rocksdb_transaction_get_for_update_cf((readOpts
                ? readOpts : this.readOptions).opts, cf.handle,
                cast(char*) key.ptr, key.length, &vlen, exclusive, &err);
        err.ensureRocks();
        GC.addRange(val, vlen);
        return cast(ubyte[]) val[0 .. vlen];
    }

    void put(ubyte[] key, ubyte[] val, WriteOptions writeOpts = null)
    {
        assert(writeOpts is null); // Not used for this operation
        char* err;
        this.handle.rocksdb_transaction_put(cast(char*) key.ptr, key.length,
                cast(char*) val.ptr, val.length, &err);
        err.ensureRocks();
    }

    void put(ubyte[] key, ubyte[] val, ColumnFamily cf, WriteOptions writeOpts = null)
    {
        assert(writeOpts is null); // Not used for this operation
        char* err;
        this.handle.rocksdb_transaction_put_cf(cf.handle, cast(char*) key.ptr,
                key.length, cast(char*) val.ptr, val.length, &err);
        err.ensureRocks();
    }

    void del(ubyte[] key, WriteOptions writeOpts = null)
    {
        assert(writeOpts is null); // Not used for this operation
        char* err;
        this.handle.rocksdb_transaction_delete(cast(char*) key.ptr, key.length, &err);
        err.ensureRocks();
    }

    void del(ubyte[] key, ColumnFamily cf, WriteOptions writeOpts = null)
    {
        assert(writeOpts is null); // Not used for this operation
        char* err;
        this.handle.rocksdb_transaction_delete_cf(cf.handle,
                cast(char*) key.ptr, key.length, &err);
        err.ensureRocks();
    }

    void merge(ubyte[] key, ubyte[] val, WriteOptions writeOpts = null)
    {
        char* err;
        this.handle.rocksdb_transaction_merge(cast(char*) key.ptr, key.length,
                cast(char*) val.ptr, val.length, &err);
        err.ensureRocks();
    }

    void merge(ubyte[] key, ubyte[] val, ColumnFamily cf, WriteOptions writeOpts = null)
    {
        char* err;
        this.handle.rocksdb_transaction_merge_cf(cf.handle,
                cast(char*) key.ptr, key.length, cast(char*) val.ptr, val.length, &err);
        err.ensureRocks();
    }

    Iterator iter(ReadOptions readOpts = null)
    {
        auto it = this.handle.rocksdb_transaction_create_iterator((readOpts
                ? readOpts : this.readOptions).opts);
        return new Iterator(it);
    }

    Iterator iter(ColumnFamily cf, ReadOptions readOpts = null)
    {
        auto it = this.handle.rocksdb_transaction_create_iterator_cf((readOpts
                ? readOpts : this.readOptions).opts, cf.handle);
        return new Iterator(it);
    }

package:
    /// Handle referencing the C object
    rocksdb_transaction_t* handle;

    /// Default options for write operations
    WriteOptions writeOptions;
    /// Default options for read operations
    ReadOptions readOptions;
}
