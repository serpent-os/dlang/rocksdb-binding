module rocksdb.optimistictransactiondb;

import core.memory : GC;
import std.string : toStringz;
import std.file : exists, isDir;

import rocksdb.writebatch : WriteBatch;
import rocksdb.binding;
import rocksdb.columnfamily : ColumnFamily;
import rocksdb.database : Database;
import rocksdb.error : ensureRocks;
import rocksdb.iterator : Iterator;
import rocksdb.options : DBOptions, ReadOptions, WriteOptions;
import rocksdb.queryable : Queryable, Snapshotable;
import rocksdb.snapshot : Snapshot;
import rocksdb.transaction : OptimisticTransactionOptions, Transaction;

/**
 * Database with Transaction support (light-weight optimistic concurrency)
 */
class OptimisticTransactionDB : Queryable
{
    /**
     * Initialize a transaction database object from a RocksDB handle
     */
    private this(rocksdb_optimistictransactiondb_t* hndl, DBOptions dbOpts)
    {
        this.handle = hndl;
        auto baseHndl = rocksdb_optimistictransactiondb_get_base_db(hndl);
        this.db = new Database(baseHndl, dbOpts);
    }

    /**
     * Close connection and clean up handle
     */
    ~this()
    {
        if (this.db !is null)
        {
            // This does the same as calling rocksdb_optimistictransactiondb_close_base_db followed by
            // rocksdb_optimistictransactiondb_close, but a database can only be closed once, so destroying
            // the base and then freeing the otxnDB struct is preferred here.
            this.db.destroy();
            this.handle.rocksdb_free();
            this.handle.destroy();
        }
    }

    /**
     * Ensure the handle is always valid
     */
    invariant
    {
        assert(this.handle !is null);
    }

    /**
     * Creates a database object and opens a read-write connection
     */
    static OptimisticTransactionDB open(string path)
    {
        DBOptions dbOpts = new DBOptions;
        dbOpts.createIfMissing = true;
        return OptimisticTransactionDB.open(dbOpts, path);
    }

    /**
     * Open a connection
     */
    static OptimisticTransactionDB open(DBOptions dbOpts, string path,
            DBOptions[string] columnFamilies = null)
    {
        rocksdb_optimistictransactiondb_t* otxnDBHndl = null;
        char* err = null;
        string[] existingColumnFamilies;

        // If there is an existing database we can check for existing column families
        if (path.exists && path.isDir)
        {
            // First check if the database has any column families
            existingColumnFamilies = []; // TODO: not supported ? TransactionDB.listColumnFamilies(dbOpts, path);
        }

        if (columnFamilies || existingColumnFamilies.length >= 1)
        {
            immutable(char*)[] columnFamilyNames;
            rocksdb_options_t*[] columnFamilyOptions;
            foreach (k; existingColumnFamilies)
            {
                columnFamilyNames ~= toStringz(k);
                columnFamilyOptions ~= columnFamilies.get(k, dbOpts).opts;
            }

            rocksdb_column_family_handle_t*[] result;
            result.length = columnFamilyNames.length;
            otxnDBHndl = rocksdb_optimistictransactiondb_open_column_families(dbOpts.opts,
                    toStringz(path), cast(int) columnFamilyNames.length,
                    columnFamilyNames.ptr, columnFamilyOptions.ptr, result.ptr, &err);
            err.ensureRocks();

            auto otxnDB = new OptimisticTransactionDB(otxnDBHndl, dbOpts);
            foreach (i, h; result)
            {
                string name = existingColumnFamilies[i];
                otxnDB.db.columnFamilies[name] = new ColumnFamily(h, name, otxnDB);
            }
            return otxnDB;
        }

        // Open database without specifying column families
        otxnDBHndl = rocksdb_optimistictransactiondb_open(dbOpts.opts, toStringz(path), &err);
        err.ensureRocks();
        return new OptimisticTransactionDB(otxnDBHndl, dbOpts);
    }

    /**
     * Create a new column family
     */
    ColumnFamily createColumnFamily(string name, DBOptions dbOpts = null)
    {
        return this.db.createColumnFamily(name, dbOpts);
    }

    /**
     * Create a snapshot
     */
    Snapshot getSnapshot()
    {
        return this.db.getSnapshot();
    }

    /**
     * Release a snapshot (used in the destructor of Snapshot)
     */
    void releaseSnapshot(Snapshot snapshot)
    {
        this.db.releaseSnapshot(snapshot);
    }

    /**
     * Create an openable snapshot
     */
    void createCheckpoint(string checkpointDir, ulong logSizeForFlush = 0)
    {
        return this.db.createCheckpoint(checkpointDir, logSizeForFlush);
    }

    /**
     * Create a new transaction
     */
    Transaction beginTransaction(Transaction oldTxn = null, WriteOptions writeOpts = null)
    {
        auto otxnOpts = new OptimisticTransactionOptions;
        auto otxnHandle = this.handle.rocksdb_optimistictransaction_begin((writeOpts ? writeOpts
                : this.db.writeOptions).opts, otxnOpts.handle, oldTxn ? oldTxn.handle : null);
        return new Transaction(otxnHandle);
    }

    /**
     * Create a new transaction
     */
    Transaction beginTransaction(OptimisticTransactionOptions otxnOpts,
            Transaction oldTxn = null, WriteOptions writeOpts = null)
    {
        assert(otxnOpts !is null);
        auto txnHandle = this.handle.rocksdb_optimistictransaction_begin((writeOpts ? writeOpts
                : this.db.writeOptions).opts, otxnOpts.handle, oldTxn ? oldTxn.handle : null);
        return new Transaction(txnHandle);
    }

    ubyte[] get(ubyte[] key, ReadOptions readOpts = null)
    {
        return this.db.get(key, readOpts);
    }

    ubyte[] get(ubyte[] key, ColumnFamily cf, ReadOptions readOpts = null)
    {
        return this.db.get(key, cf, readOpts);
    }

    void put(ubyte[] key, ubyte[] value, WriteOptions writeOpts = null)
    {
        this.db.put(key, value, writeOpts);
    }

    void put(ubyte[] key, ubyte[] value, ColumnFamily cf, WriteOptions writeOpts = null)
    {
        this.db.put(key, value, cf, writeOpts);
    }

    void del(ubyte[] key, WriteOptions writeOpts = null)
    {
        this.db.del(key, writeOpts);
    }

    void del(ubyte[] key, ColumnFamily cf, WriteOptions writeOpts = null)
    {
        this.db.del(key, cf, writeOpts);
    }

    void write(WriteBatch batch, WriteOptions writeOpts = null)
    {
        this.db.write(batch, writeOpts);
    }

    void merge(ubyte[] key, ubyte[] val, WriteOptions writeOpts = null)
    {
        this.db.merge(key, val, writeOpts);
    }

    void merge(ubyte[] key, ubyte[] val, ColumnFamily cf, WriteOptions writeOpts = null)
    {
        this.db.merge(key, val, cf, writeOpts);
    }

    Iterator iter(ReadOptions readOpts = null)
    {
        return this.db.iter(readOpts);
    }

    Iterator iter(ColumnFamily cf, ReadOptions readOpts = null)
    {
        return this.db.iter(cf, readOpts);
    }

package:
    /// Handle referencing the C object
    rocksdb_optimistictransactiondb_t* handle;

    /// Object representing the wrapped database
    Database db;
}

unittest
{
    import std.exception : assertThrown;
    import std.file : rmdirRecurse;

    auto otxnDB = OptimisticTransactionDB.open("otxnTestDB1");
    // Make sure database is cleaned up after the test
    scope (exit)
        "otxnTestDB1".rmdirRecurse();

    // Begin a transaction and rollback half of it
    auto otxn = otxnDB.beginTransaction();
    otxn.putString("key1", "value1");
    otxn.putString("key2", "value2");
    otxn.setSavepoint();
    otxn.putString("key3", "value3");
    otxn.putString("key4", "value4");
    otxn.rollbackToSavepoint();
    otxn.commit();
    otxn.destroy();

    // Check if the transaction was successful
    assert(otxnDB.getString("key1") == "value1");
    assert(otxnDB.getString("key2") == "value2");
    assert(otxnDB.getString("key3") == "");
    assert(otxnDB.getString("key4") == "");

    // Cleanup
    otxnDB.destroy();
}
