module rocksdb.options;

import std.conv : to;
import std.string : fromStringz;
import core.stdc.stdlib : cfree = free;

import rocksdb.comparator;
import rocksdb.env : Env;
import rocksdb.filterpolicy : FilterPolicy;
import rocksdb.slicetransform : SliceTransform;
import rocksdb.snapshot : Snapshot;
import rocksdb.binding;
import rocksdb.mergeoperator : MergeOperator;

enum CompressionType : int
{
    NONE = 0x0,
    SNAPPY = 0x1,
    ZLIB = 0x2,
    BZIP2 = 0x3,
    LZ4 = 0x4,
    LZ4HC = 0x5,
    XPRESS = 0x6,
    ZSTD = 0x7,
}

enum CompactionStyle : int
{
    LEVEL = 0,
    UNIVERSAL = 1,
    FIFO = 2,
}

enum ReadTier : int
{
    READ_ALL = 0x0,
    BLOCK_CACHE = 0x1,
    PERSISTED = 0x2,
}

class WriteOptions
{
    rocksdb_writeoptions_t* opts;

    this()
    {
        this.opts = rocksdb_writeoptions_create();
    }

    ~this()
    {
        rocksdb_writeoptions_destroy(this.opts);
    }

    @property void sync(bool v)
    {
        rocksdb_writeoptions_set_sync(this.opts, cast(ubyte) v);
    }

    @property void disableWAL(bool v)
    {
        rocksdb_writeoptions_disable_WAL(this.opts, cast(int) v);
    }
}

class ReadOptions
{
    rocksdb_readoptions_t* opts;

    this()
    {
        this.opts = rocksdb_readoptions_create();
    }

    ~this()
    {
        rocksdb_readoptions_destroy(this.opts);
    }

    @property void verifyChecksums(bool v)
    {
        rocksdb_readoptions_set_verify_checksums(this.opts, cast(ubyte) v);
    }

    @property void fillCache(bool v)
    {
        rocksdb_readoptions_set_fill_cache(this.opts, cast(ubyte) v);
    }

    @property void readTier(ReadTier tier)
    {
        rocksdb_readoptions_set_read_tier(this.opts, tier);
    }

    @property void tailing(bool v)
    {
        rocksdb_readoptions_set_tailing(this.opts, cast(ubyte) v);
    }

    @property void readAheadSize(size_t size)
    {
        rocksdb_readoptions_set_readahead_size(this.opts, size);
    }

    @property void snapshot(Snapshot snap)
    {
        rocksdb_readoptions_set_snapshot(this.opts, snap.handle);
    }

    @property void total_order_seek(bool seek)
    {
        rocksdb_readoptions_set_total_order_seek(this.opts, seek);
    }

    @property void prefix_same_as_start(bool prefix)
    {
        rocksdb_readoptions_set_prefix_same_as_start(this.opts, prefix);
    }
}

class BlockedBasedTableOptions
{
    rocksdb_block_based_table_options_t* opts = null;

    this()
    {
        opts = rocksdb_block_based_options_create();
    }

    ~this()
    {
        rocksdb_block_based_options_destroy(opts);
    }

    /**
     * Update the whole key filtering property
     */
    @property void wholeKeyFiltering(bool b)
    {
        rocksdb_block_based_options_set_whole_key_filtering(opts, b);
    }

    /**
     * Update the FilterPolicy for this BlockBasedTableOptions
     */
    @property void filterPolicy(FilterPolicy fp)
    {
        rocksdb_block_based_options_set_filter_policy(opts, fp.rfp);
    }
}

class DBOptions
{
    rocksdb_options_t* opts;

    this()
    {
        this.opts = rocksdb_options_create();
    }

    ~this()
    {
        rocksdb_options_destroy(this.opts);
    }

    @property void parallelism(int totalThreads)
    {
        rocksdb_options_increase_parallelism(this.opts, totalThreads);
    }

    @property void createIfMissing(bool value)
    {
        rocksdb_options_set_create_if_missing(this.opts, cast(ubyte) value);
    }

    @property void createMissingColumnFamilies(bool value)
    {
        rocksdb_options_set_create_missing_column_families(this.opts, cast(ubyte) value);
    }

    @property void errorIfExists(bool value)
    {
        rocksdb_options_set_error_if_exists(this.opts, cast(ubyte) value);
    }

    @property void paranoidChecks(bool value)
    {
        rocksdb_options_set_paranoid_checks(this.opts, cast(ubyte) value);
    }

    @property void env(Env env)
    {
        rocksdb_options_set_env(this.opts, env.env);
    }

    @property void compression(CompressionType type)
    {
        rocksdb_options_set_compression(this.opts, type);
    }

    @property void compactionStyle(CompactionStyle style)
    {
        rocksdb_options_set_compaction_style(this.opts, style);
    }

    @property void comparator(Comparator cmp)
    {
        rocksdb_options_set_comparator(this.opts, cmp.cmp);
    }

    @property void mergeOperator(MergeOperator mo)
    {
        this.opts.rocksdb_options_set_merge_operator(mo.handle);
    }

    @property void mergeOperator(rocksdb_mergeoperator_t* mo)
    {
        this.opts.rocksdb_options_set_merge_operator(mo);
    }

    @property void prefixExtractor(SliceTransform st)
    {
        if (st is null)
        {
            _prefixExtractor = null;
            return;
        }
        _prefixExtractor = st;
        rocksdb_options_set_prefix_extractor(this.opts, _prefixExtractor.rst);
    }
    /**
     * Set the factory used to create block based tables
     */
    @property void blockBasedTableFactory(BlockedBasedTableOptions bOpts)
    {
        if (bOpts is null)
        {
            _bOpts = null;
            return;
        }
        _bOpts = bOpts;
        rocksdb_options_set_block_based_table_factory(opts, _bOpts.opts);
    }

    void enableStatistics()
    {
        rocksdb_options_enable_statistics(this.opts);
    }

    string getStatisticsString()
    {
        char* cresult = rocksdb_options_statistics_get_string(this.opts);
        string result = fromStringz(cresult).to!string;
        cfree(cresult);
        return result;
    }

private:

    SliceTransform _prefixExtractor = null;
    BlockedBasedTableOptions _bOpts = null;
}

class FlushOptions
{
    this()
    {
        this.handle = rocksdb_flushoptions_create();
    }

    ~this()
    {
        if (this.handle !is null)
        {
            this.handle.rocksdb_flushoptions_destroy();
            this.handle.destroy();
        }
    }

    /**
     * Ensure the handle is always valid
     */
    invariant
    {
        assert(this.handle !is null);
    }

    /**
     * If true, the flush will wait until the flush is done.
     */
    version (RocksDBFlushOptionsGetWait) @property bool wait()
    {
        return this.handle.rocksdb_flushoptions_get_wait() != 0;
    }

    /**
     * If true, the flush will wait until the flush is done.
     */
    @property void wait(bool w)
    {
        this.handle.rocksdb_flushoptions_set_wait(w);
    }

package:
    /// Handle referencing the C object
    rocksdb_flushoptions_t* handle;
}
