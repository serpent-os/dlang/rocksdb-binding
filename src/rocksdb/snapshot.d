module rocksdb.snapshot;

import rocksdb.binding;
import rocksdb.database : Database;
import rocksdb.queryable : Snapshotable;

class Snapshot
{
    /**
     * Initialize a snapshot
     */
    package(rocksdb) this(const(rocksdb_snapshot_t)* hndl, Snapshotable s)
    {
        this.handle = hndl;
        this.s = s;
    }

    ~this()
    {
        if (this.handle)
        {
            this.s.releaseSnapshot(this);
            this.handle.destroy();
        }
    }

package:
    /// Handle referencing the C object
    const(rocksdb_snapshot_t)* handle;

    /// Instance for releasing this snapshot
    Snapshotable s;
}
