module rocksdb.mergeoperator;

import std.stdio : writeln, writefln;
import std.string : toStringz;

import rocksdb.binding;

class MergeOperator
{
    this()
    {
        this.handle = rocksdb_mergeoperator_create(cast(void*) this, &cDestructor,
                &cFullMerge, &cPartialMerge, &cDeleteValue, &cName!MergeOperator);
    }

    ~this()
    {
        if (this.handle !is null)
        {
            // TODO: this crashes: this.handle.rocksdb_mergeoperator_destroy();
            this.handle.destroy();
        }
    }

    abstract string name() const;
    abstract bool fullMerge(ubyte[] key, ubyte[] existingValue,
            ubyte[][] operandList, out ubyte[] newValue) const;
    abstract bool partialMerge(ubyte[] key, ubyte[][] operandList, out ubyte[] newValue) const;
    abstract void deleteValue(ubyte[] value) const;

package:
    /// Handle referencing the C object
    rocksdb_mergeoperator_t* handle;
}

/**
 * Global C functions taking a pointer to a MergeOperator as first argument
 */
extern (C)
{
    /**
     */
    private void cDestructor(void* self)
    {
    }

    char* cName(T)(void* self)
    {
        return cast(char*) toStringz((cast(T) self).name());
    }

    /**
      */
    private char* cFullMerge(void* self, const(char)* key, size_t keyLength,
            const(char)* existingValue, size_t existingValueLength, const(char*)* operandsList,
            const(size_t)* operandsListLength, int numOperands, ubyte* success,
            size_t* newValueLength)
    {
        ubyte[] k = cast(ubyte[]) key[0 .. keyLength];
        ubyte[] v = cast(ubyte[]) existingValue[0 .. existingValueLength];
        ubyte[][] ops;
        for (int i = 0; i < numOperands; ++i)
        {
            ops ~= cast(ubyte[]) operandsList[i][0 .. operandsListLength[i]];
        }
        ubyte[] newValue;
        *success = (cast(MergeOperator) self).fullMerge(k, v, ops, newValue);
        *newValueLength = newValue.length;
        return (cast(char[]) newValue).ptr;
    }

    /**
      */
    private char* cPartialMerge(void* self, const(char)* key, size_t keyLength,
            const(char*)* operandsList, const(size_t)* operandsListLength,
            int numOperands, ubyte* success, size_t* newValueLength)
    {
        ubyte[] k = cast(ubyte[]) key[0 .. keyLength];
        ubyte[][] ops;
        for (int i = 0; i < numOperands; ++i)
        {
            ops ~= cast(ubyte[]) operandsList[i][0 .. operandsListLength[i]];
        }
        ubyte[] newValue;
        *success = (cast(MergeOperator) self).partialMerge(k, ops, newValue);
        *newValueLength = newValue.length;
        return (cast(char[]) newValue).ptr;
    }

    /**
      */
    private void cDeleteValue(void* self, const(char)* value, size_t valueLength)
    {
    }
}
