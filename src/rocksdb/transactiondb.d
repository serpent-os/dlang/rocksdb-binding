module rocksdb.transactiondb;

import core.memory : GC;
import std.string : toStringz;
import std.file : exists, isDir;

import rocksdb.writebatch : WriteBatch;
import rocksdb.binding;
import rocksdb.columnfamily : ColumnFamily;
import rocksdb.error : ensureRocks;
import rocksdb.iterator : Iterator;
import rocksdb.options : DBOptions, ReadOptions, WriteOptions;
import rocksdb.queryable : Queryable, Snapshotable;
import rocksdb.snapshot : Snapshot;
import rocksdb.transaction : TransactionOptions, Transaction;

/**
 * Options for opening a database connection with Transaction Support
 */
class TransactionDBOptions
{
    /**
     * Initialize a new handle
     */
    this()
    {
        this.handle = rocksdb_transactiondb_options_create();
    }

    /**
     * Destroy the handle
     */
    ~this()
    {
        if (this.handle)
        {
            this.handle.rocksdb_transactiondb_options_destroy();
            this.handle.destroy();
        }
    }

    /**
     * Ensure the handle is always valid
     */
    invariant
    {
        assert(this.handle !is null);
    }

    /**
     * Specifies the maximum number of keys that can be locked at the same time per column family
     */
    @property void maxLocks(long ml)
    {
        this.handle.rocksdb_transactiondb_options_set_max_num_locks(ml);
    }

    /**
     * Increasing this value will increase the concurrency by dividing the lock table
     */
    @property void stripes(size_t s)
    {
        this.handle.rocksdb_transactiondb_options_set_num_stripes(s);
    }

    /**
     * If positive, specifies the default wait timeout in milliseconds when a transaction attempts to lock a key if not specified by TransactionOptions.lockTimeout
     * If 0, no waiting is done if a lock cannot instantly be acquired.
     * If negative, there is no timeout and will block indefinitely when acquiring a lock. Not using a timeout is not recommended as it can lead to deadlocks.
     */
    @property void transactionLockTimeout(long tlt)
    {
        this.handle.rocksdb_transactiondb_options_set_transaction_lock_timeout(tlt);
    }

    /**
     * If positive, specifies the wait timeout in milliseconds when writing a key OUTSIDE of a transaction.
     * If 0, no waiting is done if a lock cannot instantly be acquired.
     * If negative, there is no timeout and will block indefinitely when acquiring a lock.
     */
    @property void defaultLockTimeout(long dlt)
    {
        this.handle.rocksdb_transactiondb_options_set_default_lock_timeout(dlt);
    }

package:
    /// Handle referencing the C object
    rocksdb_transactiondb_options_t* handle;
}

/**
 * Database with Transaction support
 */
class TransactionDB : Queryable
{
    /**
     * Initialize a transaction database object from a RocksDB handle
     */
    private this(rocksdb_transactiondb_t* txnDBHndl, DBOptions dbOpts,
            TransactionDBOptions txnDBOpts)
    {
        this.handle = txnDBHndl;
        this.dbOptions = dbOpts;
        this.txnDBOptions = txnDBOpts;
        this.writeOptions = new WriteOptions;
        this.readOptions = new ReadOptions;
        this.columnFamilies = null;
    }

    /**
     * Close connection and clean up handle
     */
    ~this()
    {
        if (this.handle)
        {
            this.handle.rocksdb_transactiondb_close();
            this.handle.destroy();
        }
    }

    /**
     * Ensure the handle is always valid
     */
    invariant
    {
        assert(this.handle !is null);
    }

    /**
     * Creates a database object and opens a read-write connection
     */
    static TransactionDB open(string path)
    {
        DBOptions dbOpts = new DBOptions;
        dbOpts.createIfMissing = true;
        return TransactionDB.open(dbOpts, path);
    }

    /**
     * Creates a database object and opens a read-write connection
     */
    static TransactionDB open(DBOptions dbOpts, string path)
    {
        return TransactionDB.open(dbOpts, new TransactionDBOptions, path);
    }

    /**
     * Creates a database object and opens a read-write connection
     */
    static TransactionDB open(TransactionDBOptions txnDBOpts, string path)
    {
        DBOptions dbOpts = new DBOptions;
        dbOpts.createIfMissing = true;
        return TransactionDB.open(dbOpts, txnDBOpts, path);
    }

    /**
     * Open a connection
     */
    static TransactionDB open(DBOptions dbOpts, TransactionDBOptions txnDBOpts,
            string path, DBOptions[string] columnFamilies = null)
    {
        rocksdb_transactiondb_t* txnDBHndl = null;
        char* err = null;
        string[] existingColumnFamilies;

        // If there is an existing database we can check for existing column families
        if (path.exists && path.isDir)
        {
            // First check if the database has any column families
            existingColumnFamilies = []; // TODO: not supported ? TransactionDB.listColumnFamilies(dbOpts, path);
        }

        if (columnFamilies || existingColumnFamilies.length >= 1)
        {
            immutable(char*)[] columnFamilyNames;
            rocksdb_options_t*[] columnFamilyOptions;
            foreach (k; existingColumnFamilies)
            {
                columnFamilyNames ~= toStringz(k);
                columnFamilyOptions ~= columnFamilies.get(k, dbOpts).opts;
            }

            rocksdb_column_family_handle_t*[] result;
            result.length = columnFamilyNames.length;
            txnDBHndl = rocksdb_transactiondb_open_column_families(dbOpts.opts, txnDBOpts.handle, toStringz(path),
                    cast(int) columnFamilyNames.length, columnFamilyNames.ptr,
                    columnFamilyOptions.ptr, result.ptr, &err);
            err.ensureRocks();

            auto txnDB = new TransactionDB(txnDBHndl, dbOpts, txnDBOpts);
            foreach (i, h; result)
            {
                string name = existingColumnFamilies[i];
                txnDB.columnFamilies[name] = new ColumnFamily(h, name, txnDB);
            }
            return txnDB;
        }

        // Open database without specifying column families
        txnDBHndl = rocksdb_transactiondb_open(dbOpts.opts, txnDBOpts.handle,
                toStringz(path), &err);
        err.ensureRocks();
        return new TransactionDB(txnDBHndl, dbOpts, txnDBOpts);
    }

    /**
     * Create a new column family
     */
    ColumnFamily createColumnFamily(string name, DBOptions dbOpts = null)
    {
        char* err = null;
        auto cfh = this.handle.rocksdb_transactiondb_create_column_family((dbOpts
                ? dbOpts : this.dbOptions).opts, toStringz(name), &err);
        err.ensureRocks();
        return this.columnFamilies[name] = new ColumnFamily(cfh, name, this);
    }

    /**
     * Create a snapshot
     */
    Snapshot getSnapshot()
    {
        auto sHandle = this.handle.rocksdb_transactiondb_create_snapshot();
        return new Snapshot(sHandle, this);
    }

    /**
     * Release a snapshot (used in the destructor of Snapshot)
     */
    void releaseSnapshot(Snapshot snapshot)
    {
        this.handle.rocksdb_transactiondb_release_snapshot(snapshot.handle);
    }

    /**
     * Create an openable snapshot
     */
    void createCheckpoint(string checkpointDir, ulong logSizeForFlush = 0)
    {
        char* err;
        auto chkpt = this.handle.rocksdb_transactiondb_checkpoint_object_create(&err);
        err.ensureRocks();
        chkpt.rocksdb_checkpoint_create(checkpointDir.ptr, logSizeForFlush, &err);
        err.ensureRocks();
        chkpt.rocksdb_checkpoint_object_destroy();
    }

    /**
     * Create a new transaction
     */
    Transaction beginTransaction(Transaction oldTxn = null, WriteOptions writeOpts = null)
    {
        auto txnOpts = new TransactionOptions;
        auto txnHandle = this.handle.rocksdb_transaction_begin((writeOpts ? writeOpts
                : this.writeOptions).opts, txnOpts.handle, oldTxn ? oldTxn.handle : null);
        return new Transaction(txnHandle);
    }

    /**
     * Create a new transaction
     */
    Transaction beginTransaction(TransactionOptions txnOpts,
            Transaction oldTxn = null, WriteOptions writeOpts = null)
    {
        assert(txnOpts !is null);
        auto txnHandle = this.handle.rocksdb_transaction_begin((writeOpts ? writeOpts
                : this.writeOptions).opts, txnOpts.handle, oldTxn ? oldTxn.handle : null);
        return new Transaction(txnHandle);
    }

    ubyte[] get(ubyte[] key, ReadOptions readOpts = null)
    {
        char* err;
        size_t vlen;
        ubyte* val = cast(ubyte*) this.handle.rocksdb_transactiondb_get((readOpts
                ? readOpts : this.readOptions).opts, cast(char*) key.ptr,
                key.length, &vlen, &err);
        err.ensureRocks();
        GC.addRange(val, vlen);
        return cast(ubyte[]) val[0 .. vlen];
    }

    ubyte[] get(ubyte[] key, ColumnFamily cf, ReadOptions readOpts = null)
    {
        char* err;
        size_t vlen;
        ubyte* val = cast(ubyte*) this.handle.rocksdb_transactiondb_get_cf((readOpts
                ? readOpts : this.readOptions).opts, cf.handle,
                cast(char*) key.ptr, key.length, &vlen, &err);
        err.ensureRocks();
        GC.addRange(val, vlen);
        return cast(ubyte[]) val[0 .. vlen];
    }

    void put(ubyte[] key, ubyte[] value, WriteOptions writeOpts = null)
    {
        char* err;
        this.handle.rocksdb_transactiondb_put((writeOpts ? writeOpts
                : this.writeOptions).opts, cast(char*) key.ptr, key.length,
                cast(char*) value.ptr, value.length, &err);
        err.ensureRocks();
    }

    void put(ubyte[] key, ubyte[] value, ColumnFamily cf, WriteOptions writeOpts = null)
    {
        char* err;
        this.handle.rocksdb_transactiondb_put_cf((writeOpts ? writeOpts
                : this.writeOptions).opts, cf.handle, cast(char*) key.ptr,
                key.length, cast(char*) value.ptr, value.length, &err);
        err.ensureRocks();
    }

    void del(ubyte[] key, WriteOptions writeOpts = null)
    {
        char* err;
        this.handle.rocksdb_transactiondb_delete((writeOpts ? writeOpts
                : this.writeOptions).opts, cast(char*) key.ptr, key.length, &err);
        err.ensureRocks();
    }

    void del(ubyte[] key, ColumnFamily cf, WriteOptions writeOpts = null)
    {
        char* err;
        this.handle.rocksdb_transactiondb_delete_cf((writeOpts ? writeOpts
                : this.writeOptions).opts, cf.handle, cast(char*) key.ptr, key.length, &err);
        err.ensureRocks();
    }

    void write(WriteBatch batch, WriteOptions writeOpts = null)
    {
        char* err;
        this.handle.rocksdb_transactiondb_write((writeOpts ? writeOpts
                : this.writeOptions).opts, batch.handle, &err);
        err.ensureRocks();
    }

    void merge(ubyte[] key, ubyte[] val, WriteOptions writeOpts = null)
    {
        char* err;
        this.handle.rocksdb_transactiondb_merge((writeOpts ? writeOpts
                : this.writeOptions).opts, cast(char*) key.ptr, key.length,
                cast(char*) val.ptr, val.length, &err);
        err.ensureRocks();
    }

    void merge(ubyte[] key, ubyte[] val, ColumnFamily cf, WriteOptions writeOpts = null)
    {
        char* err;
        this.handle.rocksdb_transactiondb_merge_cf((writeOpts ? writeOpts
                : this.writeOptions).opts, cf.handle, cast(char*) key.ptr,
                key.length, cast(char*) val.ptr, val.length, &err);
        err.ensureRocks();
    }

    Iterator iter(ReadOptions readOpts = null)
    {
        auto it = this.handle.rocksdb_transactiondb_create_iterator((readOpts
                ? readOpts : this.readOptions).opts);
        return new Iterator(it);
    }

    Iterator iter(ColumnFamily cf, ReadOptions readOpts = null)
    {
        auto it = this.handle.rocksdb_transactiondb_create_iterator_cf((readOpts
                ? readOpts : this.readOptions).opts, cf.handle);
        return new Iterator(it);
    }

package:
    /// Handle referencing the C object
    rocksdb_transactiondb_t* handle;

    /// Options for the database
    DBOptions dbOptions;
    /// Options for the Transaction database
    TransactionDBOptions txnDBOptions;
    /// Default options for write operations
    WriteOptions writeOptions;
    /// Default options for read operations
    ReadOptions readOptions;
    /// Selected column families for this connection
    ColumnFamily[string] columnFamilies;
}

unittest
{
    import std.exception : assertThrown;
    import std.file : rmdirRecurse;

    auto txnDB = TransactionDB.open("txnTestDB1");
    // Make sure database is cleaned up after the test
    scope (exit)
        "txnTestDB1".rmdirRecurse();

    // Begin a transaction and rollback half of it
    auto txn = txnDB.beginTransaction();
    txn.putString("key1", "value1");
    txn.putString("key2", "value2");
    txn.setSavepoint();
    txn.putString("key3", "value3");
    txn.putString("key4", "value4");
    txn.rollbackToSavepoint();
    txn.commit();
    txn.destroy();

    // Check if the transaction was successful
    assert(txnDB.getString("key1") == "value1");
    assert(txnDB.getString("key2") == "value2");
    assert(txnDB.getString("key3") == "");
    assert(txnDB.getString("key4") == "");
}
