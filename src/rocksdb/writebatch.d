module rocksdb.writebatch;

import std.string : toStringz;
import std.range : zip;

import rocksdb.options, rocksdb.queryable, rocksdb.columnfamily, rocksdb.binding;

class WriteBatch : Putable, Mergeable, Deletable
{
    this()
    {
        this.handle = rocksdb_writebatch_create();
    }

    this(string frm)
    {
        this.handle = rocksdb_writebatch_create_from(toStringz(frm), frm.length);
    }

    ~this()
    {
        if (this.handle !is null)
        {
            this.handle.rocksdb_writebatch_destroy();
            this.handle.destroy();
        }
    }

    /**
     * Ensure the handle is always valid
     */
    invariant
    {
        assert(this.handle !is null);
    }

    void clear()
    {
        this.handle.rocksdb_writebatch_clear();
    }

    int count()
    {
        return this.handle.rocksdb_writebatch_count();
    }

    void put(ubyte[] key, ubyte[] value, WriteOptions opts = null)
    {
        assert(opts is null, "WriteBatch cannot use WriteOptions");
        this.handle.rocksdb_writebatch_put(cast(char*) key.ptr, key.length,
                cast(char*) value.ptr, value.length);
    }

    void put(ubyte[] key, ubyte[] value, ColumnFamily cf, WriteOptions opts = null)
    {
        assert(opts is null, "WriteBatch cannot use WriteOptions");
        this.handle.rocksdb_writebatch_put_cf(cf.handle, cast(char*) key.ptr,
                key.length, cast(char*) value.ptr, value.length);
    }

    /**
     * Update multiple keys associated with the default column family
     */
    void put(ubyte[][] keys, ubyte[][] values, WriteOptions writeOpts = null)
    {
        assert(writeOpts is null, "WriteBatch cannot use WriteOptions");
        assert(keys.length == values.length, "Number of keys and values has to be equal");
        const(char*)[] keysList, valuesList;
        const(size_t)[] keysListSizes, valuesListSizes;
        foreach (key; keys)
        {
            keysList ~= cast(const(char*)) key.ptr;
            keysListSizes ~= key.length;
        }
        foreach (value; values)
        {
            valuesList ~= cast(const(char*)) value.ptr;
            valuesListSizes ~= value.length;
        }
        this.handle.rocksdb_writebatch_putv(cast(int) keys.length, keysList.ptr,
                keysListSizes.ptr, cast(int) values.length, valuesList.ptr, valuesListSizes.ptr);
    }

    /**
     * Update multiple keys associated with the given column family
     */
    void put(ubyte[][] keys, ubyte[][] values, ColumnFamily cf, WriteOptions writeOpts = null)
    {
        assert(writeOpts is null, "WriteBatch cannot use WriteOptions");
        assert(keys.length == values.length, "Number of keys and values has to be equal");
        const(char*)[] keysList, valuesList;
        const(size_t)[] keysListSizes, valuesListSizes;
        foreach (key; keys)
        {
            keysList ~= cast(const(char*)) key.ptr;
            keysListSizes ~= key.length;
        }
        foreach (value; values)
        {
            valuesList ~= cast(const(char*)) value.ptr;
            valuesListSizes ~= value.length;
        }
        this.handle.rocksdb_writebatch_putv_cf(cf.handle, cast(int) keys.length,
                keysList.ptr, keysListSizes.ptr, cast(int) values.length,
                valuesList.ptr, valuesListSizes.ptr);
    }

    /**
     * Merge a value with the current value
     */
    void merge(ubyte[] key, ubyte[] val, WriteOptions opts = null)
    {
        assert(opts is null, "WriteBatch cannot use WriteOptions");
        this.handle.rocksdb_writebatch_merge(cast(char*) key.ptr, key.length,
                cast(char*) val.ptr, val.length);
    }

    /**
     * Merge a value with the current value
     */
    void merge(ubyte[] key, ubyte[] val, ColumnFamily cf, WriteOptions opts = null)
    {
        assert(opts is null, "WriteBatch cannot use WriteOptions");
        this.handle.rocksdb_writebatch_merge_cf(cf.handle, cast(char*) key.ptr,
                key.length, cast(char*) val.ptr, val.length);
    }

    /**
     * Merge multiple values with their current value
     */
    void merge(ubyte[][] keys, ubyte[][] values, WriteOptions writeOpts = null)
    {
        assert(writeOpts is null, "WriteBatch cannot use WriteOptions");
        assert(keys.length == values.length, "Number of keys and values has to be equal");
        const(char*)[] keysList, valuesList;
        const(size_t)[] keysListSizes, valuesListSizes;
        foreach (key; keys)
        {
            keysList ~= cast(const(char*)) key.ptr;
            keysListSizes ~= key.length;
        }
        foreach (value; values)
        {
            valuesList ~= cast(const(char*)) value.ptr;
            valuesListSizes ~= value.length;
        }
        this.handle.rocksdb_writebatch_mergev(cast(int) keys.length, keysList.ptr,
                keysListSizes.ptr, cast(int) values.length, valuesList.ptr, valuesListSizes.ptr);
    }

    /**
     * Merge multiple values with their current value
     */
    void merge(ubyte[][] keys, ubyte[][] values, ColumnFamily cf, WriteOptions writeOpts = null)
    {
        assert(writeOpts is null, "WriteBatch cannot use WriteOptions");
        assert(keys.length == values.length, "Number of keys and values has to be equal");
        const(char*)[] keysList, valuesList;
        const(size_t)[] keysListSizes, valuesListSizes;
        foreach (key; keys)
        {
            keysList ~= cast(const(char*)) key.ptr;
            keysListSizes ~= key.length;
        }
        foreach (value; values)
        {
            valuesList ~= cast(const(char*)) value.ptr;
            valuesListSizes ~= value.length;
        }
        this.handle.rocksdb_writebatch_mergev_cf(cf.handle, cast(int) keys.length,
                keysList.ptr, keysListSizes.ptr, cast(int) values.length,
                valuesList.ptr, valuesListSizes.ptr);
    }

    void del(ubyte[] key, WriteOptions opts = null)
    {
        assert(opts is null, "WriteBatch cannot use WriteOptions");
        this.handle.rocksdb_writebatch_delete(cast(char*) key.ptr, key.length);
    }

    /**
     * Remove a key associated with the given column family
     * singleDelete has the prerequisites that the key exists and was not overwritten
     * and is an experimental performance optimization for a very specific workload
     */
    version (RocksDBWriteBatchSingleDelete) void singleDel(ubyte[] key, WriteOptions writeOpts = null)
    {
        assert(writeOpts is null, "WriteBatch cannot use WriteOptions");
        this.handle.rocksdb_writebatch_singledelete(cast(char*) key.ptr, key.length);
    }

    void del(ubyte[] key, ColumnFamily cf, WriteOptions opts = null)
    {
        assert(opts is null, "WriteBatch cannot use WriteOptions");
        this.handle.rocksdb_writebatch_delete_cf(cf.handle, cast(char*) key.ptr, key.length);
    }

    /**
     * Remove a key associated with the given column family
     * singleDelete has the prerequisites that the key exists and was not overwritten
     * and is an experimental performance optimization for a very specific workload
     */
    version (RocksDBWriteBatchSingleDelete) void singleDel(ubyte[] key,
            ColumnFamily cf, WriteOptions writeOpts = null)
    {
        assert(writeOpts is null, "WriteBatch cannot use WriteOptions");
        this.handle.rocksdb_writebatch_singledelete_cf(cf.handle, cast(char*) key.ptr, key.length);
    }

    /**
     * Remove multiple keys associated with the default column family
     */
    void del(ubyte[][] keys, WriteOptions writeOpts = null)
    {
        assert(writeOpts is null, "WriteBatch cannot use WriteOptions");
        const(char*)[] keysList;
        const(size_t)[] keysListSizes;
        foreach (key; keys)
        {
            keysList ~= cast(const(char*)) key.ptr;
            keysListSizes ~= key.length;
        }
        this.handle.rocksdb_writebatch_deletev(cast(int) keys.length,
                keysList.ptr, keysListSizes.ptr);
    }

    /**
     * Remove multiple keys associated with the given column family
     */
    void del(ubyte[][] keys, ColumnFamily cf, WriteOptions writeOpts = null)
    {
        assert(writeOpts is null, "WriteBatch cannot use WriteOptions");
        const(char*)[] keysList;
        const(size_t)[] keysListSizes;
        foreach (key; keys)
        {
            keysList ~= cast(const(char*)) key.ptr;
            keysListSizes ~= key.length;
        }
        this.handle.rocksdb_writebatch_deletev_cf(cf.handle,
                cast(int) keys.length, keysList.ptr, keysListSizes.ptr);
    }

    // TODO: delete_range, delete_range_cf, delete_rangev, delete_rangev_cf

    // TODO: put_log_data, iterate, data
    // TODO: set_save_point, rollback_to_save_point, pop_save_point

package:
    /// Handle referencing the C object
    rocksdb_writebatch_t* handle;
}

// TODO: Dedup this code
class WriteBatchWithIndex : Putable, Mergeable, Deletable
{
    /**
     * Creates a WriteBatchWithIndex where no bytes are reserved up-front and
     * duplicate keys operations are retained
     */
    this()
    {
        this(false);
    }

    /**
     * Creates a WriteBatchWithIndex where no bytes are reserved up-front
     */
    this(bool overwriteKeys)
    {
        this(0, overwriteKeys);
    }

    /**
     * Creates a WriteBatchWithIndex
     */
    this(size_t reservedBytes, bool overwriteKeys)
    {
        this.handle = rocksdb_writebatch_wi_create(reservedBytes, overwriteKeys);
    }

    /*
    rocksdb_writebatch_wi_create_from has a definition but no implementation
    @disable this(string frm)
    {
        this.handle = rocksdb_writebatch_wi_create_from(toStringz(frm), frm.length);
    }
    */

    ~this()
    {
        if (this.handle !is null)
        {
            this.handle.rocksdb_writebatch_wi_destroy();
            this.handle.destroy();
        }
    }

    /**
     * Ensure the handle is always valid
     */
    invariant
    {
        assert(this.handle !is null);
    }

    void clear()
    {
        this.handle.rocksdb_writebatch_wi_clear();
    }

    int count()
    {
        return this.handle.rocksdb_writebatch_wi_count();
    }

    void put(ubyte[] key, ubyte[] value, WriteOptions opts = null)
    {
        assert(opts is null, "WriteBatch cannot use WriteOptions");
        this.handle.rocksdb_writebatch_wi_put(cast(char*) key.ptr, key.length,
                cast(char*) value.ptr, value.length);
    }

    void put(ubyte[] key, ubyte[] value, ColumnFamily cf, WriteOptions opts = null)
    {
        assert(opts is null, "WriteBatch cannot use WriteOptions");
        this.handle.rocksdb_writebatch_wi_put_cf(cf.handle,
                cast(char*) key.ptr, key.length, cast(char*) value.ptr, value.length);
    }

    /**
     * Update multiple keys associated with the default column family
     */
    void put(ubyte[][] keys, ubyte[][] values, WriteOptions writeOpts = null)
    {
        assert(writeOpts is null, "WriteBatch cannot use WriteOptions");
        assert(keys.length == values.length, "Number of keys and values has to be equal");
        const(char*)[] keysList, valuesList;
        const(size_t)[] keysListSizes, valuesListSizes;
        foreach (key; keys)
        {
            keysList ~= cast(const(char*)) key.ptr;
            keysListSizes ~= key.length;
        }
        foreach (value; values)
        {
            valuesList ~= cast(const(char*)) value.ptr;
            valuesListSizes ~= value.length;
        }
        this.handle.rocksdb_writebatch_wi_putv(cast(int) keys.length, keysList.ptr,
                keysListSizes.ptr, cast(int) values.length, valuesList.ptr, valuesListSizes.ptr);
    }

    /**
     * Update multiple keys associated with the given column family
     */
    void put(ubyte[][] keys, ubyte[][] values, ColumnFamily cf, WriteOptions writeOpts = null)
    {
        assert(writeOpts is null, "WriteBatch cannot use WriteOptions");
        assert(keys.length == values.length, "Number of keys and values has to be equal");
        const(char*)[] keysList, valuesList;
        const(size_t)[] keysListSizes, valuesListSizes;
        foreach (key; keys)
        {
            keysList ~= cast(const(char*)) key.ptr;
            keysListSizes ~= key.length;
        }
        foreach (value; values)
        {
            valuesList ~= cast(const(char*)) value.ptr;
            valuesListSizes ~= value.length;
        }
        this.handle.rocksdb_writebatch_wi_putv_cf(cf.handle, cast(int) keys.length,
                keysList.ptr, keysListSizes.ptr, cast(int) values.length,
                valuesList.ptr, valuesListSizes.ptr);
    }

    /**
     * Merge a value with the current value
     */
    void merge(ubyte[] key, ubyte[] val, WriteOptions opts = null)
    {
        assert(opts is null, "WriteBatch cannot use WriteOptions");
        this.handle.rocksdb_writebatch_wi_merge(cast(char*) key.ptr,
                key.length, cast(char*) val.ptr, val.length);
    }

    /**
     * Merge a value with the current value
     */
    void merge(ubyte[] key, ubyte[] val, ColumnFamily cf, WriteOptions opts = null)
    {
        assert(opts is null, "WriteBatch cannot use WriteOptions");
        this.handle.rocksdb_writebatch_wi_merge_cf(cf.handle,
                cast(char*) key.ptr, key.length, cast(char*) val.ptr, val.length);
    }

    /**
     * Merge multiple values with their current value
     */
    void merge(ubyte[][] keys, ubyte[][] values, WriteOptions writeOpts = null)
    {
        assert(writeOpts is null, "WriteBatch cannot use WriteOptions");
        assert(keys.length == values.length, "Number of keys and values has to be equal");
        const(char*)[] keysList, valuesList;
        const(size_t)[] keysListSizes, valuesListSizes;
        foreach (key; keys)
        {
            keysList ~= cast(const(char*)) key.ptr;
            keysListSizes ~= key.length;
        }
        foreach (value; values)
        {
            valuesList ~= cast(const(char*)) value.ptr;
            valuesListSizes ~= value.length;
        }
        this.handle.rocksdb_writebatch_wi_mergev(cast(int) keys.length,
                keysList.ptr, keysListSizes.ptr, cast(int) values.length,
                valuesList.ptr, valuesListSizes.ptr);
    }

    /**
     * Merge multiple values with their current value
     */
    void merge(ubyte[][] keys, ubyte[][] values, ColumnFamily cf, WriteOptions writeOpts = null)
    {
        assert(writeOpts is null, "WriteBatch cannot use WriteOptions");
        assert(keys.length == values.length, "Number of keys and values has to be equal");
        const(char*)[] keysList, valuesList;
        const(size_t)[] keysListSizes, valuesListSizes;
        foreach (key; keys)
        {
            keysList ~= cast(const(char*)) key.ptr;
            keysListSizes ~= key.length;
        }
        foreach (value; values)
        {
            valuesList ~= cast(const(char*)) value.ptr;
            valuesListSizes ~= value.length;
        }
        this.handle.rocksdb_writebatch_wi_mergev_cf(cf.handle, cast(int) keys.length,
                keysList.ptr, keysListSizes.ptr, cast(int) values.length,
                valuesList.ptr, valuesListSizes.ptr);
    }

    void del(ubyte[] key, WriteOptions opts = null)
    {
        assert(opts is null, "WriteBatch cannot use WriteOptions");
        this.handle.rocksdb_writebatch_wi_delete(cast(char*) key.ptr, key.length);
    }

    /**
     * Remove a key associated with the given column family
     * singleDelete has the prerequisites that the key exists and was not overwritten
     * and is an experimental performance optimization for a very specific workload
     */
    version (RocksDBWriteBatchSingleDelete) void singleDel(ubyte[] key, WriteOptions writeOpts = null)
    {
        assert(writeOpts is null, "WriteBatch cannot use WriteOptions");
        this.handle.rocksdb_writebatch_wi_singledelete(cast(char*) key.ptr, key.length);
    }

    void del(ubyte[] key, ColumnFamily cf, WriteOptions opts = null)
    {
        assert(opts is null, "WriteBatch cannot use WriteOptions");
        this.handle.rocksdb_writebatch_wi_delete_cf(cf.handle, cast(char*) key.ptr, key.length);
    }

    /**
     * Remove a key associated with the given column family
     * singleDelete has the prerequisites that the key exists and was not overwritten
     * and is an experimental performance optimization for a very specific workload
     */
    version (RocksDBWriteBatchSingleDelete) void singleDel(ubyte[] key,
            ColumnFamily cf, WriteOptions writeOpts = null)
    {
        assert(writeOpts is null, "WriteBatch cannot use WriteOptions");
        this.handle.rocksdb_writebatch_wi_singledelete_cf(cf.handle,
                cast(char*) key.ptr, key.length);
    }

    /**
     * Remove multiple keys associated with the default column family
     */
    void del(ubyte[][] keys, WriteOptions writeOpts = null)
    {
        assert(writeOpts is null, "WriteBatch cannot use WriteOptions");
        const(char*)[] keysList;
        const(size_t)[] keysListSizes;
        foreach (key; keys)
        {
            keysList ~= cast(const(char*)) key.ptr;
            keysListSizes ~= key.length;
        }
        this.handle.rocksdb_writebatch_wi_deletev(cast(int) keys.length,
                keysList.ptr, keysListSizes.ptr);
    }

    /**
     * Remove multiple keys associated with the given column family
     */
    void del(ubyte[][] keys, ColumnFamily cf, WriteOptions writeOpts = null)
    {
        assert(writeOpts is null, "WriteBatch cannot use WriteOptions");
        const(char*)[] keysList;
        const(size_t)[] keysListSizes;
        foreach (key; keys)
        {
            keysList ~= cast(const(char*)) key.ptr;
            keysListSizes ~= key.length;
        }
        this.handle.rocksdb_writebatch_wi_deletev_cf(cf.handle,
                cast(int) keys.length, keysList.ptr, keysListSizes.ptr);
    }

    // TODO: delete_range, delete_range_cf, delete_rangev, delete_rangev_cf

    // TODO: put_log_data, iterate, data
    // TODO: set_save_point, rollback_to_save_point, pop_save_point

    // TODO: get_from_batch, get_from_batch_cf
    // TODO: get_from_batch_and_db, get_from_batch_and_db_cf

    // TODO: writeWithIndex in database implementations

    // TODO: create_iterator_with_base
    // TODO: create_iterator_with_base_cf

package:
    /// Handle referencing the C object
    rocksdb_writebatch_wi_t* handle;
}
