/**
 * Interfaces to RockDB operations
 *
 * This is not present in the official RockDB library, but keeps database
 * implementations maintainable. Delete has been renamed to del because it is a
 * keyword and del is consistent with get and put (also matches std.net.curl).
 */
module rocksdb.queryable;

import std.conv : to;

import rocksdb.columnfamily : ColumnFamily;
import rocksdb.iterator : Iterator;
import rocksdb.options : ReadOptions, WriteOptions;
import rocksdb.snapshot : Snapshot;
import rocksdb.writebatch : WriteBatch;

interface Getable
{
    /**
     * Retrieve a key associated with the default column family
     */
    ubyte[] get(ubyte[] key, ReadOptions opts = null);

    /**
     * Retrieve a key associated with the given column family
     */
    ubyte[] get(ubyte[] key, ColumnFamily cf, ReadOptions opts = null);

    /**
     * Retrieve a key associated with the default column family as a string
     */
    final string getString(string key, ReadOptions opts = null)
    {
        return cast(string) this.get(cast(ubyte[]) key, opts);
    }

    /**
     * Retrieve a key associated with the given column family as a string
     */
    final string getString(string key, ColumnFamily cf, ReadOptions opts = null)
    {
        return cast(string) this.get(cast(ubyte[]) key, cf, opts);
    }
}

interface Putable
{
    /**
     * Update a key associated with the default column family
     */
    void put(ubyte[] key, ubyte[] value, WriteOptions opts = null);

    /**
     * Update a key associated with the given column family
     */
    void put(ubyte[] key, ubyte[] value, ColumnFamily cf, WriteOptions opts = null);

    /**
     * Update a key associated with the default column family as a string
     */
    final void putString(string key, string value, WriteOptions opts = null)
    {
        this.put(cast(ubyte[]) key, cast(ubyte[]) value, opts);
    }
    /**
     * Update a key associated with the given column family as a string
     */
    final void putString(string key, string value, ColumnFamily cf, WriteOptions opts = null)
    {
        this.put(cast(ubyte[]) key, cast(ubyte[]) value, cf, opts);
    }
}

interface Deletable
{
    /**
     * Remove a key associated with the default column family
     */
    void del(ubyte[] key, WriteOptions opts = null);

    /**
     * Remove a key associated with the given column family
     */
    void del(ubyte[] key, ColumnFamily cf, WriteOptions opts = null);

    /**
     * Remove a key associated with the default column family as a string
     */
    final void delString(string key, WriteOptions opts = null)
    {
        this.del(cast(ubyte[]) key, opts);
    }

    /**
     * Remove a key associated with the given column family as a string
     */
    final void delString(string key, ColumnFamily cf, WriteOptions opts = null)
    {
        this.del(cast(ubyte[]) key, cf, opts);
    }
}

interface Writable
{
    /**
     * Write a batch of puts and dels
     */
    void write(WriteBatch batch, WriteOptions opts = null);

    /**
     * Run a delegate taking a batch
     */
    final void withBatch(void delegate(WriteBatch) dg, WriteOptions writeOpts = null)
    {
        WriteBatch batch = new WriteBatch;
        scope (exit)
            destroy(batch);
        scope (success)
            this.write(batch, writeOpts);
        dg(batch);
    }
}

interface Mergeable
{
    /**
     * Merge a value with the current value
     */
    void merge(ubyte[] key, ubyte[] value, WriteOptions opts = null);

    /**
     * Merge a value with the current value
     */
    void merge(ubyte[] key, ubyte[] value, ColumnFamily cf, WriteOptions opts = null);

    /**
     * Merge a value with the current value as a string
     */
    final void mergeString(string key, string value, WriteOptions opts = null)
    {
        this.merge(cast(ubyte[]) key, cast(ubyte[]) value, opts);
    }

    /**
     * Merge a value with the current value as a string
     */
    final void mergeString(string key, string value, ColumnFamily cf, WriteOptions opts = null)
    {
        this.merge(cast(ubyte[]) key, cast(ubyte[]) value, cf, opts);
    }
}

interface Iterable
{
    /**
     * Create an iterator over the keys and values
     */
    Iterator iter(ReadOptions readOpts = null);

    /**
     * Create an iterator over the keys and values
     */
    Iterator iter(ColumnFamily cf, ReadOptions readOpts = null);

    /**
     * Run a delegate taking an iterator
     */
    final void withIter(void delegate(Iterator) dg, ReadOptions readOpts = null)
    {
        Iterator iter = this.iter(readOpts);
        scope (exit)
            destroy(iter);
        dg(iter);
    }

    /**
     * Run a delegate taking an iterator
     */
    final void withIter(void delegate(Iterator) dg, ColumnFamily cf, ReadOptions readOpts = null)
    {
        Iterator iter = this.iter(cf, readOpts);
        scope (exit)
            destroy(iter);
        dg(iter);
    }
}

interface Snapshotable
{
    /**
     * Create a snapshot
     */
    Snapshot getSnapshot();

    /**
     * Release a snapshot
     */
    void releaseSnapshot(Snapshot snapshot);
}

interface Checkpointable
{
    /**
     * Create an openable snapshot of the database.
     * checkpointDir should contain an absolute path and should not exist.
     */
    void createCheckpoint(string checkpointDir, ulong logSizeForFlush = 0);
}

/**
 * Interface that provides get, put and del operations
 *
 * Queryables can be the subject of a ColumnFamily
 */
interface Queryable : Getable, Putable, Deletable, Writable, Mergeable,
    Iterable, Snapshotable, Checkpointable
{

}
