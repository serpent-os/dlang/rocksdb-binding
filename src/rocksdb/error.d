module rocksdb.error;

import std.string : fromStringz, toStringz;
import std.format : format;

pragma(inline) pure void ensureRocks(char* err)
{
    if (err)
    {
        throw new Exception(format("Error: %s", fromStringz(err)));
    }
}
