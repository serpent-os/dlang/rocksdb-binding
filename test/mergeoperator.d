unittest
{
    import std.algorithm.iteration : reduce;
    import std.file : rmdirRecurse;
    import std.stdio : writefln;

    import rocksdb.database;
    import rocksdb.options;
    import rocksdb.mergeoperator;

    // Implement dummy operator
    class SumOperator : MergeOperator
    {
        override string name() const
        {
            return "SumOperator";
        }

        override bool fullMerge(ubyte[] key, ubyte[] existingValue,
                ubyte[][] operandList, out ubyte[] newValue) const
        {
            newValue = existingValue;
            foreach (operand; operandList)
            {
                if (operand.length != newValue.length)
                    return false;
                newValue[] += operand[];
            }
            return true;
        }

        override bool partialMerge(ubyte[] key, ubyte[][] operandList, out ubyte[] newValue) const
        {
            if (operandList.length == 0)
                return false;
            newValue = operandList[0];
            foreach (operand; operandList[1 .. $])
            {
                if (operand.length != newValue.length)
                    return false;
                newValue[] += operand[];
            }
            return true;
        }

        override void deleteValue(ubyte[] value) const
        {
        }
    }

    // Create connection
    auto dbOpts = new DBOptions;
    dbOpts.createIfMissing = true;
    dbOpts.mergeOperator = new SumOperator;
    string path = "mergeOperatorTestDB1";
    auto db = Database.open(dbOpts, path);
    scope (exit)
        path.rmdirRecurse();

    // Do a couple of merge operations
    ubyte[] key = [0];
    db.put(key, [1]);
    db.merge(key, [2]);
    db.merge(key, [3]);

    // Check if they succeeded
    assert(db.get(key) == [6]);

    // Cleanup
    db.destroy();
}
