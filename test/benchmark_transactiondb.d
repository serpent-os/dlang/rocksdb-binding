unittest
{
    import std.file : rmdirRecurse;
    import std.stdio : writefln;
    import std.conv : to;
    import std.datetime.stopwatch : benchmark;

    import rocksdb.writebatch : WriteBatch;
    import rocksdb.options : CompressionType, DBOptions;
    import rocksdb.transactiondb : TransactionDBOptions, TransactionDB;
    import rocksdb.transaction : TransactionOptions;
    import rocksdb.env : Env;

    writefln("Benchmarking TransactionDB");

    // Benchmarking environment and options
    auto env = new Env;
    env.backgroundThreads = 2;
    env.highPriorityBackgroundThreads = 1;
    auto dbOpts = new DBOptions;
    dbOpts.createIfMissing = true;
    dbOpts.errorIfExists = false;
    dbOpts.compression = CompressionType.NONE;
    dbOpts.env = env;
    auto txnDBOpts = new TransactionDBOptions;
    txnDBOpts.stripes = 2;

    // Make sure database is clean
    string path = "benchmarkTransactionDB";
    auto db = TransactionDB.open(dbOpts, txnDBOpts, path);
    scope (exit)
        path.rmdirRecurse();

    // Benchmarks
    void writeBench(int times)
    {
        for (int i = 0; i < times; i++)
        {
            db.putString(i.to!string, i.to!string);
        }
    }

    void writeTxnBench(int times)
    {
        auto txnOpts = new TransactionOptions;
        auto txn = db.beginTransaction(txnOpts);
        for (int i = 0; i < times; i++)
        {
            txn.putString(i.to!string, i.to!string);
        }
        txn.commit();
        txn.destroy();
    }

    void readBench(int times)
    {
        for (int i = 0; i < times; i++)
        {
            assert(db.getString(i.to!string) == i.to!string);
        }
    }

    void writeBatchBench(int times)
    {
        db.withBatch((batch) {
            for (int i = 0; i < times; i++)
            {
                batch.putString(i.to!string, i.to!string);
            }

            assert(batch.count() == times);
        });
    }

    auto writeRes = benchmark!(() => writeBench(100_000))(1);
    writefln("  writing a val 100000 times: %sms", writeRes[0].split!().msecs);

    auto writeTxnRes = benchmark!(() => writeTxnBench(100_000))(1);
    writefln("  writing a val 100000 times inside a transaction: %sms",
            writeTxnRes[0].split!().msecs);

    auto readRes = benchmark!(() => readBench(100_000))(1);
    writefln("  reading a val 100000 times: %sms", readRes[0].split!().msecs);

    auto writeBatchRes = benchmark!(() => writeBatchBench(100_000))(1);
    writefln("  batch writing 100000 vals: %sms", writeBatchRes[0].split!().msecs);

    // Cleanup
    db.destroy();
}
