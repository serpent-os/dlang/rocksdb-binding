unittest
{
    import std.exception : assertThrown;
    import std.file : rmdirRecurse;
    import std.format : format;
    import std.stdio : writeln;

    import rocksdb.database : Database;

    // Create a database
    string path = "checkpointTestDB1";
    scope (exit)
        path.rmdirRecurse();
    auto db = Database.open(path);

    // Put some values
    for (int i = 0; i < 10; ++i)
    {
        db.putString(format("key%d", i), format("value%d", i));
    }

    // Create a checkpoint
    string checkpointPath = "checkpointTestDB1Save";
    scope (exit)
        checkpointPath.rmdirRecurse();
    db.createCheckpoint(checkpointPath);

    // Put some more values
    for (int i = 10; i < 20; ++i)
    {
        db.putString(format("key%d", i), format("value%d", i));
    }

    // Open the checkpoint as read-only
    auto cp = Database.open(checkpointPath);

    // Check that only the first batch of values is present
    for (int i = 0; i < 10; ++i)
    {
        assert(cp.getString(format("key%d", i)) == format("value%d", i));
    }
    for (int i = 10; i < 20; ++i)
    {
        assert(cp.getString(format("key%d", i)) is null);
    }

    // Cleanup
    db.destroy();
    cp.destroy();
}
