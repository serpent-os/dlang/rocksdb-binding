unittest
{
    import std.file : rmdirRecurse;
    import std.stdio : writefln;
    import std.datetime.stopwatch : benchmark;
    import rocksdb.env : Env;
    import rocksdb.options : CompressionType, DBOptions;
    import rocksdb.database : Database;
    import std.conv : to;

    writefln("Benchmarking Database");

    auto env = new Env;
    env.backgroundThreads = 2;
    env.highPriorityBackgroundThreads = 1;

    auto dbOpts = new DBOptions;
    dbOpts.createIfMissing = true;
    dbOpts.errorIfExists = false;
    dbOpts.compression = CompressionType.NONE;
    dbOpts.env = env;

    // Make sure database is clean
    auto db = Database.open(dbOpts, "testDB1");
    scope (exit)
        "testDB1".rmdirRecurse();

    // Test string putting and getting
    db.putString("key", "val");
    assert(db.getString("key") == "val");
    db.putString("key", "val2");
    assert(db.getString("key") == "val2");

    ubyte[] key = ['\x00', '\x00'];
    ubyte[] val = ['\x01', '\x02'];

    // Test byte based putting / getting
    db.put(key, val);
    assert(db.get(key) == val);
    db.del(key);

    // Benchmarks
    void writeBench(int times)
    {
        for (int i = 0; i < times; i++)
        {
            db.putString(i.to!string, i.to!string);
        }
    }

    void readBench(int times)
    {
        for (int i = 0; i < times; i++)
        {
            assert(db.getString(i.to!string) == i.to!string);
        }
    }

    auto writeRes = benchmark!(() => writeBench(100_000))(1);
    writefln("  writing a val 100000 times: %sms", writeRes[0].split!().msecs);

    auto readRes = benchmark!(() => readBench(100_000))(1);
    writefln("  reading a val 100000 times: %sms", readRes[0].split!().msecs);

    // Test batch
    void writeBatchBench(int times)
    {
        db.withBatch((batch) {
            for (int i = 0; i < times; i++)
            {
                batch.putString(i.to!string, i.to!string);
            }

            assert(batch.count() == times);
        });
    }

    auto writeBatchRes = benchmark!(() => writeBatchBench(100_000))(1);
    writefln("  batch writing 100000 vals: %sms", writeBatchRes[0].split!().msecs);
    readBench(100_000);

    // Test scanning from a location
    bool found = false;
    auto iterFrom = db.iter();
    iterFrom.seek("key");
    foreach (key, val; iterFrom)
    {
        assert(val == "val2");
        assert(!found);
        found = true;
    }
    iterFrom.destroy();
    assert(found);

    found = false;
    int keyCount = 0;
    auto iter = db.iter();

    foreach (key, val; iter)
    {
        if (key == "key")
        {
            assert(val == "val2");
            found = true;
        }
        keyCount++;
    }
    iter.destroy();
    assert(found);
    assert(keyCount == 100001);
    db.destroy();
}
